class TPIDRegulator:
    
    def defaultDF(self, x, y):
        return x - y

    """
    dF - function that calculates difference between target and current
    """
    def __init__(self, iK, pK, dK, target = None, maxSumError = 10 ** 9, minSumError = -(10 ** 9), maxOutput = 10 ** 9, minOutput = -(10 ** 9), dF = None):
        self.iK = iK
        self.pK = pK
        self.dK = dK
        
        self.target = None
        
        self.prevError = 0.0
        self.sumError = 0.0

        self.maxSumError = maxSumError
        self.minSumError = minSumError

        self.output = None
        
        self.maxOutput = maxOutput
        self.minOutput = minOutput

        if dF is not None:
            self.dF = dF
        else:   
            self.dF = self.defaultDF

    def setTarget(self, target):
        self.target = target
    
    def resetAll(self):
        self.prevError = 0.0
        self.sumError = 0.0
    
    def process(self, current):
        error = self.dF(self.target, current) 
        errorDiff = error - self.prevError

        self.sumError += error
        self.prevError = error

        self.sumError = min(self.maxSumError, max(self.minSumError, self.sumError))
        
        output = self.iK * self.sumError + self.pK * error + self.dK * errorDiff
        
        output = min(self.maxOutput, max(self.minOutput, output))

        return output

