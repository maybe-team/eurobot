import math

from tf.transformations import euler_from_quaternion

"""
Simple realization of Math
"""
class TPoint2D(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "(x: " + str(self.x) + "; y: " + str(self.y) + ")"
    
    def __mul__(self, k):
        return TPoint2D(self.x * k, self.y * k)

    def __add__(self, obj):
        return TPoint2D(self.x + obj.x, self.y + obj.y)

    def __repr__(self):
        return self.__str__()

class TVector2D(TPoint2D):
    def __init__(self, x, y):
        TPoint2D.__init__(self, x, y)

    def length(self):
        x = self.x
        y = self.y
        return math.sqrt(x * x + y * y)
    
    def normalize(self):
        res = TVector2D(self.x, self.y)
        if self.length() == 0.0:
            return res

        res = self * (1.0 / self.length())
        return res

    def __mul__(self, k):
        return TVector2D(self.x * k, self.y * k)

    def __add__(self, obj):
        return TVector2D(self.x + obj.x, self.y + obj.y)


    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ") ->"

class TSegment(object):
    # A, B: TPoint2D
    def __init__(self, A, B):
        self.A = A
        self.B = B

def degToRad(val):
    return val * 0.0174532

def radToDeg(val):
    return val * 57.29577

def quaternionToAngle(q):
    return euler_from_quaternion(q)

def normalizeAngle(angle):
    while angle >= degToRad(360.0):
        angle -= degToRad(360.0)
    while angle < 0:
        angle += degToRad(360.0)
    return angle

def ortVector(vec):
    if vec.x == 0.0:
        return TVector2D(cmp(vec.y, 0), 0.0)
    if vec.y == 0.0:
        return TVector2D(0.0, cmp(0, vec.x))

    ort = TVector2D(-(vec.y / vec.x), 1.0)
    ort *= cmp(0, vec.x)

    return ort

def vectorFromPoints(p1, p2):
    result = TVector2D(p2.x - p1.x, p2.y - p1.y)
    return result

def vectorProd(v1, v2):
    prod = (v1.x * v2.x + v1.y * v2.y)
    return prod

def vectorCos(v1, v2):
    prod = vectorProd(v1, v2)
    length = v1.length() * v2.length()
    
    if length == 0.0:
        return 0.0

    return prod / length

def vectorPseudo(v1, v2):
    pseudo = ((v1.x * v2.y) - (v1.y * v2.x))
    return pseudo

def rotateVector(vector, angle):
    result = TVector2D(0.0, 0.0)
    result.x = vector.x * math.cos(angle) - vector.y * math.sin(angle)
    result.y = vector.x * math.sin(angle) + vector.y * math.cos(angle)
    return result

def distFromSegment(seg, point):
    vecAB = vectorFromPoints(seg.A, seg.B)
    vecBA = vectorFromPoints(seg.B, seg.A)

    vecAP = vectorFromPoints(seg.A, point)
    vecBP = vectorFromPoints(seg.B, point)
    
    if vecAB.length() == 0.0 or vecAP.length() == 0.0 or vecBP.length() == 0.0:
        return min(vecAP.length(), vecBP.length())

    angle1cos = vectorProd(vecAB, vecAP) / (vecAB.length() * vecAP.length())
    angle2cos = vectorProd(vecBA, vecBP) / (vecBA.length() * vecBP.length())

    if angle1cos > 0 and angle2cos > 0:
        dst = abs(0.5 * vectorPseudo(vecAB, vecAP) / vecAB.length())
    else:
        dst = min(vecAP.length(), vecBP.length())

    return dst
