class TRobot:
    def __init__(self):
        self.x = None
        self.y = None
        self.orient = None
        self.theta = None
    
    def __str__(self):
        return "%s %s %s" % (self.x, self.y, self.theta)    
