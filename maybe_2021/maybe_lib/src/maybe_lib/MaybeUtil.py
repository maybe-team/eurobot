import rospy, threading

def waitForPublisher(publisher):
    while publisher.get_num_connections() < 1:
        pass

    return

def locatedPrint(self, obj):
    if hasattr(self, "name"):
        return "[" + self.name + "]:\t" + obj.__str__()
    else:
        return obj.__str__()

def debug(self, obj):
    if self.ctx.data["scheduler"]["debug"]:
        print locatedPrint(self, obj)


class TRuntimeContext(object):
   
    def __init__(self, base):
        self.data = base
        self._lock = threading.Lock()
        
