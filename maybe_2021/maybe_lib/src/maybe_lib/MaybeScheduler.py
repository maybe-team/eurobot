import threading
import time

from maybe_lib.MaybeUtil import debug, TRuntimeContext

# TODO: use python logging

"""
Base class of all Scheduler tasks
Takes name (MUST BE UNIQUE inside single scheduler)
symbDeps - list of dependecies (names of tasks that this task SHOULD WAIT FOR)
every node has methods:
trigger and _ping - first accepts ok from other node, second tries to execute this node
"""
class ITaskNode(object):
    
    def __init__(self, name, symbDeps):
        
        self.name = name
        self.symbDeps = list(symbDeps)
        self.ctx = TRuntimeContext(dict())

        self._deps = dict()
        self._triggers = dict()
        
        self.__thread = threading.Thread()
        self.__started = False
        self.__success = False
        self.__error = False
        
        self._lock = threading.Lock()

        self._errorInstance = None

    def status(self):
        if not self.__started:
            return "waiting"
        
        if self.__success:
            return "success"
        
        if self.__error:
            return "error"

        return "running"
    
    """
    Checks wether this node is ready to execute 
    """
    def check(self):
        for key in self.symbDeps:
            if not key in self._deps:
                raise RuntimeError("Task " + key + " required")
            else:
                dep = self._deps[key]
                if dep["lock"]: 
                    return False
        debug(self, "ready")
        return True
    
    def join(self):
        
        self.__thread.join()
         
    def task(self):
        raise RuntimeError("task(self) method is not set")

    def _task(self):

        try:
            self.task()
        except Exception as e:
            debug(self, "Exception " + e.args[0])
            self.__error = True
            self._errorInstance = e
            pass
        else: 
            self.__success = True
        
        for t in self._triggers:
            if not self._triggers[t]["node"].isTriggered(self.name):
                self._triggers[t]["node"].trigger(self.name)
                debug(self, "wanna ping " + t)
                self.pingChild(t)

    def execute(self):
        self.__thread = threading.Thread(target = self._task, name = self.name)
        self.__thread.daemon = True
        self.__thread.start()

    def _ping(self, source = ""): 
        if (not self.__started) and self.check():
            debug(self, "started by " + source)
            self.__started = True
            self.execute()
    """
    Called by other node - allow this node to execute (it needs agreement with all parents)
    """
    def trigger(self, triggerName): 
        if triggerName in self._deps:
            self._deps[triggerName]["lock"] = False
    
    def isTriggered(self, triggerName):
        return not self._deps[triggerName]["lock"]
    
    """
    Call trigger of this node's child
    """
    def triggerChild(self, childName):
        if childName in self._triggers:
            if not self._triggers[childName]["node"].isTriggered(self.name):    
                self._triggers[childName]["node"].trigger(self.name)
    """
    Tries to execute child
    """
    def pingChild(self, childName):        
        if childName in self._triggers:
            child = self._triggers[childName]["node"] 
            if child.isTriggered(self.name):
                debug(self, "trying to lock " + childName)
                with child._lock:
                    debug(self, "lock " + childName)
                    self._triggers[childName]["node"]._ping(self.name)  
                debug(self, "unlock " + childName)


class TScheduler(object):
    """
    Root is one node to rule them ALL
    Sends trigger and ping to all nodes at start
    Waits all nodes

    ctx.data["scheduler"] takes
    rootRate: float root node checks all nodes with rootRate interval
    rootControlEnalbed: root pings all nodes by itself

    """
    class TRootNode(ITaskNode):
        def __init__(self, name, symbDeps, scheduler):
            ITaskNode.__init__(self, name, symbDeps)
            self.scheduler = scheduler

        def task(self):
            debug(self, "Starting root node...")
            
            for x in self._triggers:
                self.triggerChild(x)
                self.pingChild(x)

            allComplete = False

            while not allComplete:
                allComplete = True
                time.sleep(self.ctx.data["scheduler"]["rootRate"])
                for x in self._triggers:
                    status = self._triggers[x]["node"].status() 
                    if status == "waiting":
                        allComplete = False
                        if (self.ctx.data["scheduler"]["rootControlEnabled"]):
                            self.pingChild(x)
                    if status == "error":
                        error = self._triggers[x]["node"]._errorInstance
                        raise error

                    elif status == "running":
                        allComplete = False
                    else:
                        pass
            
    def __init__(self, ctx):
        self._tasks = dict()
        self.ctx = ctx
        self.root = TScheduler.TRootNode("root", [], scheduler = self) 

        self.root.ctx = ctx
        self._tasks["root"] = self.root

        self.name = "scheduler"

    """
    Add task to scheduler
    """
    def addTask(self, task):
        task.symbDeps.append("root")
        task.ctx = self.ctx
        self._tasks[task.name] = task
    
    """
    Build graph of execution from list of dependencies
    """
    def build(self):
        debug(self, "building dependencies...")
        for name in self._tasks:
            task = self._tasks[name]
            for depName in task.symbDeps:
                if not depName in self._tasks:
                    raise RuntimeError("Cannot build dependencies, task \"" + name + "\" requires task \"" + "\"")
                else:
                    task._deps[depName] = {"lock": True, "node": self._tasks[depName]}
                    self._tasks[depName]._triggers[name] = {"node": task}

    """
    Run the graph from build
    """
    def start(self):
        debug(self, "starting scheduler...")
        self.root._ping(self.name)
        self.root.join()
        
        if self.root.status() == "success":
            debug(self, "success")
        elif self.root.status() == "error":
            debug(self, "error")
            raise self.root._errorInstance
        else:
            debug(self, "WTF?")
