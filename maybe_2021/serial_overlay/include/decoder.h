/*
    Functions that wee need to encode and decode messages to STM
    Solves problem with 0x00 byte
*/

/*
    Function that converts number to string hex
    UNUSED but maybe helpful for DEBUG
    TODO: delete if you want
*/
void maybe_convert(int inp, char * p, int * k) {
    (*k) = 0;
    while (inp > 0) {
        int d = inp % 16;
        if (d < 9) {
            p[(*k)++] = '0' + d;
        } else {
            p[(*k)++] = 'a' + (10 - d);
        }
        inp = inp / 16;
    }
    p[(*k)++] = 'x';
    p[(*k)++] = '0';
    p[(*k)++] = '\n';
    for (int i = 0; i < (*k) / 2; ++i) {
        char tmp = p[(*k) - i - 1];
        p[(*k) - i - 1] = p[i];
        p[i] = tmp;
    }
    return;
}

/*
    splits byte to left byte [lb] and right byte rb
    byte = abcdefgh
    lb   = 1111efgh
    rb   = abcd1111
*/
void split(char byte, char * lb, char * rb) {
    (*lb) = ((byte << 4) >> 4) | 0b11110000;
    (*rb) = 0b00001111  | ((byte >> 4) << 4);
}

/*
    merge lb and rb from split
*/
void merge(char lb, char rb, char * byte) {
    (*byte) = (lb & 0b00001111) | (rb & 0b11110000);
}

/*
    encodes message from [from] of length [len]
    writes it to [to]
*/
void encode(char * from, char * to, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        char lb, rb;
        split((*from), &lb, &rb);
        (*(to++)) = lb;
        (*(to++)) = rb;
        ++from;
    }
}

/*
    decodes message from [from]
    writes it to [to] of length [len]
    NOTE: function takes length of DECODED message (NOT length of from)
*/
void decode(char * from, char * to, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        char r;
        merge(*(from), *(from + 1), &r);
        (*to) = r;
        ++to;
        from += 2;
    }
}
