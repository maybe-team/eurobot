/*
    Here you should place structs from stm c code
    They will be used in TSTMOverlay as IN or OUT
*/


/*
    The only my struct for string response
*/
const size_t BUFFER_SIZE = 256;

typedef struct {
    char buffer[BUFFER_SIZE];
} __attribute__((packed)) buffer_t;

/*
    Original STM structs
*/

typedef struct {
    float v1;
    float v2;
    float v3;
} __attribute__((packed)) cmd_wheel_speed_t; 

typedef struct {
    float pwm1;
    float pwm2;
    float pwm3;
} __attribute__((packed)) cmd_pwm_t; 

typedef struct {
        float x;
        float y;
        float theta;
} __attribute__((packed)) cmd_set_coord_t;

typedef struct {
        float vx;
        float vy;
        float wz;
} __attribute__((packed)) cmd_speed_t;

typedef struct {
        float x;
        float y;
} __attribute__((packed)) cmd_set_xy_t;

typedef struct {
        float theta;
} __attribute__((packed)) cmd_set_theta_t;

typedef struct {
        int id;
        int pos;
        int vel;
} __attribute__((packed)) cmd_set_servo_pose_t;

typedef struct {
        int id;
        int angle;
        int vel;
} __attribute__((packed)) cmd_set_servo_angle_t;
