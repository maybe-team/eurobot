#ifndef UPDATE_H
#define UPDATE_H

#include <serial/serial.h>
#include <ros/ros.h>

/*
    Declaration of TSTMOverlay class
    TSTMOverlay handles cmd code
    IN  - type of input message
    OUT - type of output message
    set - Serial port pointer

    Template
    TSTMOverlay<IN, OUT> name(&ser, 0x00); 0x00 - command code
    IN input;
    OUT output;
    name.send(IN, &OUT)
*/

template<typename IN, typename OUT>
class TSTMOverlay {
public:
    TSTMOverlay(serial::Serial *, int);
    void send(const IN & ex, OUT * out);
private:
    int cmd;
    serial::Serial * ser;
};

#include <overlay.hpp>

#endif
