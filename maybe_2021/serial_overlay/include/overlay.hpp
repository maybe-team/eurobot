#include <iostream>

#include <std_msgs/String.h>
#include <ros/ros.h>
#include <overlay.h>

template<typename IN, typename OUT>
TSTMOverlay<IN, OUT>::TSTMOverlay(serial::Serial * ser, int cmd) {
    this->cmd = cmd;
    this->ser = ser;
}
     
template<typename IN, typename OUT>
void TSTMOverlay<IN, OUT>::send(const IN & ex, OUT * out) {
    const void * begin = &ex;

    int command = this->cmd;
    char buffer[sizeof(IN) + 2];
    char buffer2[(sizeof(IN) + 2) * 2];
    
    // write command to first byte of buffer
    buffer[0] = char(command);

    // then we are looking at IN ex object like it is array of bytes; write these bytes to buffer
    for (int i = 1; i <= sizeof(IN); ++i) {
        buffer[i] = (*(char *)(begin + i - 1)); 
    }
    
    // encode buffer; write result to buffer2
    encode(buffer, buffer2, sizeof(IN) + 2); 
    
    // send buffer2 to STM
    ser->write(buffer2);

    // wait for response
    while (!ser->available()) {
//      ros::Duration(0.05).sleep(); 
    }
 
    
    std_msgs::String result;
    result.data = ser->read(ser->available());
    
    // result is STM response as a STRING; now we convert it to OUT and copy it to out
    memcpy(out, (OUT *)((void *)result.data.c_str()), result.data.size());
}
