#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <serial/serial.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <stm_types.h>
#include <decoder.h>
#include <overlay.h>
#include <serial_overlay/GetWheel.h>
#include <serial_overlay/SetCoord.h>
#include <serial_overlay/GetCoord.h>
#include <serial_overlay/GetSpeed.h>
#include <serial_overlay/SetSpeed.h>
#include <serial_overlay/GetPWM.h>
#include <serial_overlay/SetPWM.h>
#include <serial_overlay/SetXY.h>
#include <serial_overlay/SetTheta.h>
#include <serial_overlay/SetServo.h>
#include <serial_overlay/SetServoAngle.h>
#include <geometry_msgs/Point.h>
/*
    RosCPP node that makes services and communicates with STM
    ser - Serial port, that you must send to TSTMOverlay objects
*/

serial::Serial ser;

/*
    Services that use TSTMOverlay
*/

bool set_coord(serial_overlay::SetCoord::Request & req, serial_overlay::SetCoord::Response & resp) {
    cmd_set_coord_t inp_coord;
    inp_coord.x = (float) req.x;
    inp_coord.y = (float) req.y;
    inp_coord.theta = (float) req.theta;
    
    
    TSTMOverlay<cmd_set_coord_t, buffer_t> send_coord(&ser, 0x06);
 
    buffer_t set_coord_res;
    send_coord.send(inp_coord, &set_coord_res);
    
    return true;
}

bool set_xy(serial_overlay::SetXY::Request & req, serial_overlay::SetXY::Response & resp) {
    cmd_set_xy_t inp_coord;
    inp_coord.x = (float) req.x;
    inp_coord.y = (float) req.y;
    TSTMOverlay<cmd_set_xy_t, buffer_t> send_coord(&ser, 0x0b);
 
    buffer_t set_coord_res;
    send_coord.send(inp_coord, &set_coord_res);
    return true;
}

bool set_theta(serial_overlay::SetTheta::Request & req, serial_overlay::SetTheta::Response & resp) {
    cmd_set_theta_t inp_coord;
    inp_coord.theta = (float) req.theta;
    TSTMOverlay<cmd_set_theta_t, buffer_t> send_coord(&ser, 0x0c);
 
    buffer_t set_coord_res;
    send_coord.send(inp_coord, &set_coord_res);
    return true;
}

bool get_coord(serial_overlay::GetCoord::Request & req, serial_overlay::GetCoord::Response & resp) {
    TSTMOverlay<int, cmd_set_coord_t> send_coord(&ser, 0x07);
    cmd_set_coord_t get_coord_res;

    send_coord.send(req.command, &get_coord_res);
    

    resp.x = get_coord_res.x;
    resp.y = get_coord_res.y;
    resp.theta = get_coord_res.theta;
    return true;
}

void publish_odom(ros::Publisher & pub) {
    
    nav_msgs::Odometry odom;
    
    TSTMOverlay<int, cmd_set_coord_t> send_coord(&ser, 0x07);
    cmd_set_coord_t get_coord_res;
    send_coord.send(1, &get_coord_res);
    
    TSTMOverlay<int, cmd_speed_t> get_speed(&ser, 0x05);
    cmd_speed_t get_speed_res;
    get_speed.send(1, &get_speed_res);
    
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "map";
    odom.child_frame_id = "odom";
    odom.pose.pose.position.x = get_coord_res.x;
    odom.pose.pose.position.y = get_coord_res.y;
    geometry_msgs::Quaternion quat = tf::createQuaternionMsgFromYaw(get_coord_res.theta);
    odom.pose.pose.orientation = quat;
    
    odom.twist.twist.linear.x = get_speed_res.vx;
    odom.twist.twist.linear.y = get_speed_res.vy;
    odom.twist.twist.angular.z = get_speed_res.wz;

    pub.publish(odom);
}

bool get_speed(serial_overlay::GetSpeed::Request & req, serial_overlay::GetSpeed::Response & resp) {
    TSTMOverlay<int, cmd_speed_t> get_speed(&ser, 0x05);
    
    cmd_speed_t get_speed_res;
    
    get_speed.send(req.command, &get_speed_res);
    
    resp.vx = get_speed_res.vx;
    resp.vy = get_speed_res.vy;
    resp.wz = get_speed_res.wz;

    return true;
}

void publish_speed(ros::Publisher & pub) {
    TSTMOverlay<int, cmd_speed_t> send_coord(&ser, 0x05);
    cmd_speed_t get_speed_res;

    send_coord.send(1, &get_speed_res);

    geometry_msgs::Point result;
    result.x = get_speed_res.vx;
    result.y = get_speed_res.vy;
    result.z = get_speed_res.wz;

    pub.publish(result);
}


bool get_wheel_speed(serial_overlay::GetWheel::Request & req, serial_overlay::GetWheel::Response & resp) {
    TSTMOverlay<int, cmd_wheel_speed_t> get_speed(&ser, 0x03);
    
    cmd_wheel_speed_t get_speed_res;
    
    get_speed.send(req.command, &get_speed_res);
    
    resp.v1 = get_speed_res.v1;
    resp.v2 = get_speed_res.v2;
    resp.v3 = get_speed_res.v3;

    return true;
}

bool set_speed(serial_overlay::SetSpeed::Request & req, serial_overlay::SetSpeed::Response & resp) {    
    TSTMOverlay<cmd_speed_t, buffer_t> send_speed(&ser, 0x04);
    
    cmd_speed_t inp_speed;

    inp_speed.vx = req.vx;
    inp_speed.vy = req.vy;
    inp_speed.wz = req.wz;
    
    buffer_t set_speed_res;
    send_speed.send(inp_speed, &set_speed_res);
    
    
    resp.res = 0;

    return true;
}

bool get_pid_output(serial_overlay::GetWheel::Request & req, serial_overlay::GetWheel::Response & resp) {
    TSTMOverlay<int, cmd_pwm_t> get_speed(&ser, 0x09);
    
    cmd_pwm_t get_speed_res;
    
    get_speed.send(req.command, &get_speed_res);
    
    resp.v1 = get_speed_res.pwm1;
    resp.v2 = get_speed_res.pwm2;
    resp.v3 = get_speed_res.pwm3;

    return true;
}

bool get_pid_setpoint(serial_overlay::GetWheel::Request & req, serial_overlay::GetWheel::Response & resp) {
    TSTMOverlay<int, cmd_pwm_t> get_speed(&ser, 0x0a);
    
    cmd_pwm_t get_speed_res;
    
    get_speed.send(req.command, &get_speed_res);
    
    resp.v1 = get_speed_res.pwm1;
    resp.v2 = get_speed_res.pwm2;
    resp.v3 = get_speed_res.pwm3;

    return true;
}

bool set_pid_coef(serial_overlay::SetPWM::Request & req, serial_overlay::SetPWM::Response & resp) {
    TSTMOverlay<cmd_pwm_t, buffer_t> send_pwm(&ser, 0x08);

    cmd_pwm_t inp_pwm;

    inp_pwm.pwm1   = req.pwm1;
    inp_pwm.pwm2   = req.pwm2;
    inp_pwm.pwm3   = req.pwm3;

    buffer_t set_pwm_res;

    send_pwm.send(inp_pwm, &set_pwm_res);


    resp.res = 0;

    return true;
}


bool set_servo(serial_overlay::SetServo::Request & req, serial_overlay::SetServo::Response & resp) {
    TSTMOverlay<cmd_set_servo_pose_t, buffer_t> send_servo(&ser, 0x0d);

    cmd_set_servo_pose_t servo_pose;

    servo_pose.id = req.id;
    servo_pose.pos = req.pos;
    servo_pose.vel = req.vel;
    
    buffer_t set_servo_res;

    send_servo.send(servo_pose, &set_servo_res);


    resp.res = 0;

    return true;
}

bool set_servo_angle(serial_overlay::SetServoAngle::Request & req, serial_overlay::SetServoAngle::Response & resp) {
    TSTMOverlay<cmd_set_servo_angle_t, buffer_t> send_servo_angle(&ser, 0x0e);

    cmd_set_servo_angle_t servo_angle;

    servo_angle.id = req.id;
    servo_angle.angle = req.angle;
    servo_angle.vel = req.vel;
    
    buffer_t set_servo_angle_res;

    send_servo_angle.send(servo_angle, &set_servo_angle_res);


    resp.res = 0;

    return true;
}

bool set_pwm(serial_overlay::SetPWM::Request & req, serial_overlay::SetPWM::Response & resp) {
    TSTMOverlay<cmd_pwm_t, buffer_t> send_pwm(&ser, 0x02);

    cmd_pwm_t inp_pwm;

    inp_pwm.pwm1   = req.pwm3;
    inp_pwm.pwm2   = req.pwm2;
    inp_pwm.pwm3   = req.pwm3;

    buffer_t set_pwm_res;

    send_pwm.send(inp_pwm, &set_pwm_res);


    resp.res = 0;

    return true;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "serial_overlay_node");
    ros::NodeHandle nh;
    
    std::string port = "/dev/ttyUSB0";
    int rate = 115200, timeout = 1000;
    try {
        ser.setPort(port);
        ser.setBaudrate(rate);
        serial::Timeout to = serial::Timeout::simpleTimeout(timeout);
        ser.setTimeout(to);
        ser.open();
    } catch (serial::IOException& e) {
        ROS_ERROR_STREAM("Unable to open port ");
        return -1;
    }

    if (ser.isOpen()) {
        ROS_INFO_STREAM("Serial Port initialized");
    } else {
        return -1;
    }

    
    ros::ServiceServer get_wheel_speed_service = nh.advertiseService("/serial_overlay/get_wheel_speed", get_wheel_speed);
    ros::ServiceServer set_pid_coef_service = nh.advertiseService("/serial_overlay/set_pid_coef", set_pid_coef);
    ros::ServiceServer get_pid_output_service = nh.advertiseService("/serial_overlay/get_pid_output", get_pid_output);
    ros::ServiceServer get_pid_setpoint_service = nh.advertiseService("/serial_overlay/get_pid_setpoint", get_pid_setpoint);


    ros::ServiceServer set_servo_service = nh.advertiseService("/serial_overlay/set_servo", set_servo);
    ros::ServiceServer set_servo_angle_service = nh.advertiseService("/serial_overlay/set_servo_angle", set_servo_angle);
    ros::ServiceServer set_coord_service = nh.advertiseService("/serial_overlay/set_coord", set_coord);
    ros::ServiceServer get_coord_service = nh.advertiseService("/serial_overlay/get_coord", get_coord);
    ros::ServiceServer get_speed_service = nh.advertiseService("/serial_overlay/get_speed", get_speed);
    ros::ServiceServer set_speed_service = nh.advertiseService("/serial_overlay/set_speed", set_speed);
    ros::ServiceServer set_pwm_service   = nh.advertiseService("/serial_overlay/set_pwm",   set_pwm);
    ros::ServiceServer set_xy_service = nh.advertiseService("/serial_overlay/set_xy", set_xy);
    ros::ServiceServer set_theta_service = nh.advertiseService("/serial_overlay/set_theta", set_theta);

    ros::Publisher odom_pub = nh.advertise<nav_msgs::Odometry>("/odom", 1000);
    double delta, old_time = ros::Time::now().toSec(), begin_time;
    while (ros::ok()) {
        begin_time = ros::Time::now().toSec() ;
        publish_odom(odom_pub);
        delta = (ros::Time::now().toSec() - old_time); 
        old_time = ros::Time::now().toSec();
        ros::spinOnce();
        delta = (ros::Time::now().toSec() - begin_time); 
        ros::Duration(std::max(0.0, 0.01 - delta)).sleep();
    }
    /*
        MANUAL USE OF TSTMOverlay UNCOMMENT IF YOU HAVE PROBLEMS.
    */
    
    /*
    cmd_set_coord_t inp_coord;
    float x, y, alpha;
    
    std::cin >> x;
    std::cin >> y;
    std::cin >> alpha;
    
    inp_coord.x = x;
    inp_coord.y = y;
    inp_coord.alpha = alpha;
    
    
    TSTMOverlay<cmd_set_coord_t, buffer_t> send_coord(&ser, 0x71);
    TSTMOverlay<int, cmd_set_coord_t> get_coord(&ser, 0x72);
 
    buffer_t set_coord_res;
    send_coord.send(inp_coord, &set_coord_res);
    
    std::cout << set_coord_res.buffer << '\n';

    cmd_set_coord_t get_coord_res;
    get_coord.send(1, &get_coord_res);

    std::cout << get_coord_res.x << '\n';
    std::cout << get_coord_res.y << '\n';
    std::cout << get_coord_res.alpha << '\n';

    TSTMOverlay<cmd_set_speed_t, buffer_t> send_speed(&ser, 0x61);
    TSTMOverlay<int, decltype(odometry_ctrl_t::inst_local_speed)> get_speed(&ser, 0x78);

    cmd_set_speed_t inp_speed;
    float vx, vy, wz;

    std::cin >> vx >> vy >> wz;
    
    inp_speed.vx = vx;
    inp_speed.vy = vy;
    inp_speed.wz = wz;
    
    buffer_t set_speed_res;
    send_speed.send(inp_speed, &set_speed_res);

    std::cout << set_speed_res.buffer << '\n';
    
    odometry_ctrl_t get_speed_res;
    
    get_speed.send(1, &(get_speed_res.inst_local_speed));
    std::cout << get_speed_res.inst_local_speed[0] << '\n';
    std::cout << get_speed_res.inst_local_speed[1] << '\n';
    std::cout << get_speed_res.inst_local_speed[2] << '\n';
    
    */

    return 0;
}
