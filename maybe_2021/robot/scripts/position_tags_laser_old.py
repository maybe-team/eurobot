#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import Marker
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from tf.transformations import euler_from_quaternion
from math import atan2, sin, cos, hypot
roll = pitch = theta_odom = 0
ranges_from_lidar = ()
x_lidar_global = 1.36
y_lidar_global = 0.96
theta_lidar = 270
deg_to_rad = 0.01745
rad_to_deg = 57.2957
x_odom = y_odom = 0
dis_eps = 0.08
angle_spaces = 30
update_odom_rate = 10
odom_rate_counter = 0

def normalizeAngle(angle):
    while angle >= 360:
        angle -= 360
    while angle < 0:
        angle += 360
    return angle

rospy.init_node('position_tag_lidar')
r = rospy.Rate(6)
point_pub = rospy.Publisher('position_tag_lidar', Point, queue_size=1)
odom_update_pub = rospy.Publisher('odom_update', Point, queue_size=1)
def callback_lidar(msg):
    global ranges_from_lidar
    ranges_from_lidar = msg.ranges

def callback_odom (msg):
    global x_lidar_global, y_lidar_global, roll, pitch, theta_odom, x_odom, y_odom
    x_odom = msg.pose.pose.position.x
    y_odom = msg.pose.pose.position.y
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, theta_odom) = euler_from_quaternion (orientation_list)
    theta_odom = int(theta_odom * rad_to_deg)
    if theta_odom < 0:
        theta_odom += 360


def lidar_localization():
    global ranges_from_lidar, deg_to_rad, rad_to_deg, x_lidar_global, y_lidar_global, theta_lidar, theta_odom, x_odom, y_odom, dis_eps, angle_spaces, update_odom_rate, odom_rate_counter
    count_cycle = 0
    global_tag_point = Point()
    odom_update = Point()

    #print "#######################################"
    #print x_lidar_global, y_lidar_global, theta_lidar
    global_angle_list = [0.0] * 3
    global_angle_list[0] = (atan2(x_lidar_global + 0.09, 2 - y_lidar_global - 0.05)) * rad_to_deg
    global_angle_list[1] = (atan2(y_lidar_global - 0.05, x_lidar_global + 0.09))  * rad_to_deg + 90
    if y_lidar_global >= 1:
        global_angle_list[2] = (atan2(3 - x_lidar_global + 0.09, y_lidar_global - 1)) * rad_to_deg + 180
    else:
        global_angle_list[2] = (atan2(1 - y_lidar_global, 3 - x_lidar_global + 0.09)) * rad_to_deg + 270

    print "global angle list ", global_angle_list

    dis_calc_list = [0.0] * 3
    dis_calc_list[0] = ((2 - y_lidar_global - 0.05) ** 2 + (x_lidar_global + 0.09) ** 2) ** 0.5
    dis_calc_list[1] = ((y_lidar_global - 0.05) ** 2 + (x_lidar_global + 0.09) ** 2) ** 0.5

    if y_lidar_global >= 1:
        dis_calc_list[2] = ((y_lidar_global - 1) ** 2 + (3 - x_lidar_global + 0.09) ** 2) ** 0.5
    else:
        dis_calc_list[2] = ((1 - y_lidar_global) ** 2 + (3 - x_lidar_global + 0.09) ** 2) ** 0.5


    local_angle_list = [normalizeAngle(round(360 - (theta_lidar - angle) - angle_spaces / 2)) for angle in global_angle_list]

    dis_list = [0.0] * 3
    lidar_real_list = [0.0] * 3

    print local_angle_list
    print "dis_calc ", dis_calc_list
    while count_cycle < angle_spaces:

        local_angle_list = [normalizeAngle(angle) for angle in local_angle_list]

        dis_search_list = [0.9925785 * ((ranges_from_lidar[int(angle)])**0.9644696) + 0.05 for angle in local_angle_list]

        for i in range(3):
            if dis_calc_list[i] - dis_eps < dis_search_list[i] < dis_calc_list[i] + dis_eps:
                if abs(dis_search_list[i] - dis_calc_list[i]) < abs(dis_list[i] - dis_calc_list[i]):
                    dis_list[i] = dis_search_list[i]
                    lidar_real_list[i] = local_angle_list[i]


        local_angle_list = [angle + 1 for angle in local_angle_list]
        count_cycle += 1


    theta_lidar_list = [normalizeAngle(360 - lidar_real_list[i] + global_angle_list[i]) for i in range(3)]

    x_list = [0.0] * 3
    y_list = [0.0] * 3

    useful_global_angle_list = [0.0] * 3
    useful_global_angle_list[0] = global_angle_list[0]
    useful_global_angle_list[1] = 90 - (global_angle_list[1] - 90)
    useful_global_angle_list[2] = (global_angle_list[2] - 180) if y_lidar_global >= 1 else (90 - (global_angle_list[2] - 270))

    x_constants = [(1, -0.09), (1, -0.09), (-1, 3 + 0.09)]
    y_constants = [(-1, 2 - 0.05), (1, 0.05), (1 if y_lidar_global >= 1 else -1, 1)]

    working_list = []

    for i in range(3):
        if dis_list[i] > 0 and i != 1:
            x_list[i] = x_constants[i][0] * (sin((useful_global_angle_list[i]) * deg_to_rad) * dis_list[i]) + x_constants[i][1]
            y_list[i] = y_constants[i][0] * (cos((useful_global_angle_list[i]) * deg_to_rad) * dis_list[i]) + y_constants[i][1]
            working_list.append(i)
        elif dis_list[i] > 0  and i == 1:
            working_list.append(i)
            x_list[1] = (cos((global_angle_list[1] - 90)*deg_to_rad) * dis_list[1]) - 0.09
            y_list[1] = (sin((global_angle_list[1] - 90)*deg_to_rad) * dis_list[1]) + 0.05

    print "working shits ", working_list
    if len(working_list) >= 2:
        x_lidar_global = y_lidar_global = theta_lidar = 0.0
        for i in working_list:
                x_lidar_global += x_list[i]
                y_lidar_global += y_list[i]
                theta_lidar += theta_lidar_list[i]

        theta_lidar /= len(working_list)
        x_lidar_global /= len(working_list)
        y_lidar_global /= len(working_list)
        angle_spaces = 20
        dis_eps = 0.08
        odom_rate_counter += 1
        if odom_rate_counter == update_odom_rate:
            odom_rate_counter = 0
            odom_update.x = x_lidar_global
            odom_update.y = y_lidar_global
            odom_update.z = theta_lidar
            odom_update_pub.publish(odom_update)
    else:
        print "ODOM used"
        theta_lidar = theta_odom
        x_lidar_global = x_odom
        y_lidar_global = y_odom
        angle_spaces = 50
        dis_eps = 0.2

    print x_lidar_global, y_lidar_global, theta_lidar
    print "x1 ", x_list[0], " x2 ", x_list[1], " x3 ", x_list[2]
    print "y1 ", y_list[0], " y2 ", y_list[1], " y3 ", y_list[2]
    global_tag_point.x = x_lidar_global
    global_tag_point.y = y_lidar_global
    global_tag_point.z = theta_lidar
    point_pub.publish(global_tag_point)


lidar = rospy.Subscriber('/scan', LaserScan, callback_lidar)
odom_sub = rospy.Subscriber("/odom", Odometry, callback_odom)
rospy.sleep(1.)
while not rospy.is_shutdown():
    lidar_localization()
    r.sleep()

