#!/usr/bin/env python


import rospy
import math
from main_robot.msg import servo_msg
from std_msgs.msg import Int16MultiArray

poses = Int16MultiArray()
poses.data = [512]*9
poses.data[1] = 872
poses.data[3] = 32
poses.data[4] = 14
poses.data[5] = 692
poses.data[6] = 372
poses.data[7] = 372
poses.data[8] = 0




rospy.init_node('servo_state')
r = rospy.Rate(10)

def servo_callback(msg):
    global poses
    idd = int(msg.id)
    poses.data[idd] = msg.pose
    print poses.data[1],',',poses.data[3],',',poses.data[4],',',poses.data[5],',',poses.data[6],',',poses.data[7],',',poses.data[8]

servo_sub = rospy.Subscriber('cmd_servo_robot2', servo_msg, servo_callback)
while not rospy.is_shutdown():
    r.sleep()
