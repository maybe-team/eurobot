#!/usr/bin/env python

import rospy

from maybe_lib.msg import MaybeCup, MaybeCupList

from geometry_msgs.msg import PoseStamped, Pose2D


rospy.init_node("cups")

class TCup:
    def __init__(self, x, y, color, inflation):
        self.x = x
        self.y = y
        self.color = color
        self.inflation = inflation
class TCupManager:
    def __init__(self):
        self.cups = [
            TCup(67, 10, "r", 0.20),
            TCup(233, 10, "g", 0.20),
            TCup(30, 40, "r", 0.20),
            TCup(95, 40, "g", 0.20),
            TCup(205, 40, "r", 0.20),
            TCup(270, 40, "g", 0.20),
            TCup(45, 51, "r", 0.20),
            TCup(255, 51, "g", 0.24),
            TCup(110, 80, "r", 0.235),
            TCup(190, 80, "g", 0.235),
            TCup(45, 108, "r", 0.24),
            TCup(255, 108, "g", 0.24),
            TCup(30, 120, "g", 0.20),
            TCup(127, 120, "g", 0.22),
            TCup(173, 120, "r", 0.22),
            TCup(270, 120, "r", 0.20),
            TCup(106.5, 165, "g", 0.20),
            TCup(133.5, 165, "r", 0.20),
            TCup(166.5, 165, "r", 0.20),
            TCup(193.5, 165, "g", 0.20),
            TCup(100.5, 195.5, "g", 0.20),
            TCup(139.5, 195.5, "r", 0.20),
            TCup(160.5, 195.5, "r", 0.20),
            TCup(199.5, 195.5, "g", 0.20)
        ]
        self.cupsPublisher = rospy.Publisher("/mb/cups", MaybeCupList, queue_size = 1)   
    
    def fakeSub(self, msg):
        self.fakePose = msg

    def pubCups(self):
        pubCups = MaybeCupList()

        for x in self.cups:
            cupMsg = MaybeCup()
            cupMsg.x = x.x / 100.0
            cupMsg.y = 2.0 - (x.y / 100.0)
            cupMsg.inflation = x.inflation
            cupMsg.color = x.color
            pubCups.cups.append(cupMsg)

        self.cupsPublisher.publish(pubCups)


cupManager = TCupManager()

while not rospy.is_shutdown():
    cupManager.pubCups()
    rospy.sleep(0.01)
