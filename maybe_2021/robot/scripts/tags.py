#!/usr/bin/env python
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point, PoseStamped
import rospy
import math
rospy.init_node('register')
publisher1 = rospy.Publisher('marker1', Marker, queue_size=1)
publisher2 = rospy.Publisher('marker2', Marker, queue_size=1)
publisher3 = rospy.Publisher('marker3', Marker, queue_size=1)
publisher4 = rospy.Publisher('main_marker', Marker, queue_size=1)

def point_callback(msg):
    marker4 = Marker()
    marker4.header.frame_id = "map"
    marker4.type = marker4.CYLINDER
    marker4.action = marker4.ADD
    marker4.scale.x = 0.4
    marker4.scale.y = 0.4
    marker4.scale.z = 0.1
    marker4.color.a = 1.0
    marker4.color.r = 1.0
    marker4.color.g = 0.0
    marker4.color.b = 0.0
    marker4.pose.orientation.w = 1.0
    marker4.pose.position.x = msg.pose.position.x
    marker4.pose.position.y = msg.pose.position.y
    marker4.pose.position.z = 0.5
    publisher4.publish(marker4)


def tags_fun():
    marker1 = Marker()
    marker1.header.frame_id = "map"
    marker1.type = marker1.CYLINDER
    marker1.action = marker1.ADD
    marker1.scale.x = 0.1
    marker1.scale.y = 0.1
    marker1.scale.z = 0.45
    marker1.color.a = 1.0
    marker1.color.r = 0.0
    marker1.color.g = 1.0
    marker1.color.b = 0.0
    marker1.pose.orientation.w = 1.0
    marker1.pose.position.x = -0.09
    marker1.pose.position.y = 1.95
    marker1.pose.position.z = 0.225

    marker2 = Marker()

    marker2.header.frame_id = "map"
    marker2.type = marker2.CYLINDER
    marker2.action = marker2.ADD
    marker2.scale.x = 0.1
    marker2.scale.y = 0.1
    marker2.scale.z = 0.45
    marker2.color.a = 1.0
    marker2.color.r = 0.0
    marker2.color.g = 1.0
    marker2.color.b = 0.0
    marker2.pose.orientation.w = 1.0
    marker2.pose.position.x = -0.09
    marker2.pose.position.y = 0.05
    marker2.pose.position.z = 0.225

    marker3 = Marker()

    marker3.header.frame_id = "map"
    marker3.type = marker3.CYLINDER
    marker3.action = marker3.ADD
    marker3.scale.x = 0.1
    marker3.scale.y = 0.1
    marker3.scale.z = 0.45
    marker3.color.a = 1.0
    marker3.color.r = 0.0
    marker3.color.g = 1.0
    marker3.color.b = 0.0
    marker3.pose.orientation.w = 1.0
    marker3.pose.position.x = 3.09
    marker3.pose.position.y = 1
    marker3.pose.position.z = 0.225

    publisher1.publish(marker1)
    publisher2.publish(marker2)
    publisher3.publish(marker3)

sub_point = rospy.Subscriber('aruco/robot4', PoseStamped, point_callback)
while not rospy.is_shutdown():

   tags_fun()
