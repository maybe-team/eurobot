#!/usr/bin/env python

import rospy

from geometry_msgs.msg import Pose2D, PoseStamped

rospy.init_node("pipe")

def callback(msg):
    global pub
    x = PoseStamped()
    x.pose.position.x = msg.x
    x.pose.position.y = msg.y
    pub.publish(x)


rospy.Subscriber("/pose_big_robot", Pose2D, callback)
pub = rospy.Publisher("/aruco/robot4", PoseStamped, queue_size = 1)

rospy.spin()
