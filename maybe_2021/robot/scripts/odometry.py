#!/usr/bin/env python
import rospy
import math
import time
import tf
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from std_msgs.msg import Float32, Int32, Float64, Int32
from nav_msgs.msg import Odometry
from main_robot.msg import motors_speeds
from math import sin, cos, atan2, hypot, pi
rospy.init_node('odom')
pub = rospy.Publisher("/odom", Odometry, queue_size=1)
x_hui = y_hui = 0
rast_from_center = 0.136824
deg_to_rad = 0.0174532
rad_to_deg = 57.29577
ticks_to_meter = 0.00003895
v_list = [0.0]*3
global_angle = time_old = odom_quat = 0.0
global_x = rospy.get_param('global_x')
global_y = rospy.get_param('global_y')
theta = (rospy.get_param('theta')) * deg_to_rad
time_old = 0.0
real_delta = 0.0
odom_broadcaster = tf.TransformBroadcaster()

robot_radius = 0.125

def odom_update_callback(msg):
    global global_x, global_y, theta
    global_x = msg.x
    global_y = msg.y


def odom_update_theta_callback(msg):
    global theta
    theta = msg.data * deg_to_rad

def normalizeAngle(angle):
    while angle >= pi*2:
        angle -= pi*2
    while angle < 0:
        angle += pi*2
    return angle

odom_quat = tf.transformations.quaternion_from_euler(0, 0, theta)
#print theta * rad_to_deg
print odom_quat
def callback_real_speeds(msg):
    global  v_list, global_angle, theta, time_old, x_hui, y_hui , global_x, global_y, real_delta, rast_from_center, rad_to_deg, deg_to_rad, ticks_to_meter
    current_time = rospy.Time.now()
    t = rospy.Time.from_sec(time.time())
    real_delta = ((t.to_nsec())/1000000 - time_old) * 0.001
    time_old = (t.to_nsec())/1000000
    v_list[0] = msg.motor1
    v_list[1] = msg.motor2
    v_list[2] = msg.motor3

    delta_x = ((-v_list[0] + v_list[1] * cos(5.24) + v_list[2]*  cos(1.05)) * real_delta) * 1.032
    delta_y = (v_list[1] * sin(5.24) + v_list[2] * sin(1.05)) * real_delta

    v_x = delta_x / real_delta
    v_y = delta_y / real_delta

    delta_theta = ((v_list[0] + v_list[1] + v_list[2]) / rast_from_center) * real_delta / 1.886

    if delta_y == 0:
        if delta_x >= 0:
            global_angle = 0
        else:
            global_angle = 3.14
    else:
        global_angle = atan2(delta_x, delta_y)

    v_global = hypot(v_x, v_y)
    #print 'v_global = ', v_global
    theta = normalizeAngle(theta)
    theta += delta_theta
    global_x -= (v_global * real_delta) * sin(-theta + global_angle)
    global_y -= (v_global * real_delta) * cos(-theta + global_angle)


    odom = Odometry()
    odom_quat = tf.transformations.quaternion_from_euler(0, 0, theta)
    #print theta * rad_to_deg
    print odom_quat

    odom_broadcaster.sendTransform(
        (global_x, global_y, 0.0),
        odom_quat,
        current_time,
        "base_link",
        "odom"
    )
    con = [0] * 36

    con[0] = con[7] = con[14] = con[21] = con[28] = con[35] = 0.001
    odom.pose.covariance = con
    odom.twist.covariance = con
    odom.header.stamp = current_time
    odom.header.frame_id = "map"
    odom.pose.pose = Pose(Point(global_x, global_y, 0.), Quaternion(*odom_quat))
    odom.child_frame_id = "odom"
    odom.twist.twist = Twist(Vector3(v_x, v_y, v_global), Vector3(0, 0, 0))
    pub.publish(odom)

real_speeds = rospy.Subscriber('/v_real', motors_speeds, callback_real_speeds)
odom_update_theta = rospy.Subscriber('/odom_update_theta', Float32, odom_update_theta_callback)
odom_update_sub = rospy.Subscriber('odom_update_xy', Point, odom_update_callback)
rospy.spin()
