#!/usr/bin/env python

import rospy, re

from std_msgs.msg import Int8, String
from main_robot.msg import servo_msg
from maybe_lib.srv import ServoCommand 
from serial_overlay.srv import SetServo
from maybe_lib.msg import ServoPoses

rospy.init_node('servo_commands')

servo_poses_pub = rospy.Publisher("/servo/poses", ServoPoses, queue_size = 1)
servoPoses = ServoPoses()

def servo_poses_sub_callback(msg):
    for i in range (10):
        servoPoses.poses[i] = msg.poses[i]

class TServoCommands:
    def __init__(self):
        self.servo_send_proxy = rospy.ServiceProxy("serial_overlay/set_servo", SetServo)
        self.control_servo = SetServo()

    def servo(self, idd, pose, speed):
        self.control_servo.id = idd
        self.control_servo.pos = pose
        self.control_servo.vel = speed
        self.servo_send_proxy(self.control_servo.id, self.control_servo.pos, self.control_servo.vel)
        #TODO: remove in competition
        global servoPoses
        servoPoses.poses[idd] = pose
        servo_poses_pub.publish(servoPoses)
        rospy.sleep(0.008)

    def manipulator_speeds(self, pose1, speed, pose3, speed3, pose4, speed4, pose5, speed5, pose6, speed6):
        self.servo(1, pose1, speed1)
        self.servo(3, pose3, speed3)
        self.servo(4, pose4, speed4)
        self.servo(5, pose5, speed5)
        self.servo(6, pose6, speed6)

    def manipulator(self, pose1, pose3, pose4, pose5, pose6, speed):
        self.servo(1, pose1, speed)
        self.servo(3, pose3, speed)
        self.servo(4, pose4, speed)
        self.servo(5, pose5, speed)
        self.servo(6, pose6, speed)
    
    def home(self):
        self.manipulator(767, 57, 912, 918, 512, 200)
        rospy.sleep(2.)
        self.manipulator(917, 57, 912, 928, 512, 200)
    
    def pretake(self):
        self.manipulator(782, 432, 487, 1023, 357, 500)

    def flag_up(self):
        self.servo(7, 100, 200)

    def flag_down(self):
        self.servo(7, 100, 200)
   
    def catcher_on(self):
        self.servo(8, 100, 100)

    def catcher_off(self):
        self.servo(8, 100, 100)

    def cell_1(self):
        self.servo(9, 100, 100)
    
    def cell_2(self):
        self.servo(9, 100, 100)
    
    def cell_3(self):
        self.servo(9, 100, 100)
    
    def cell_4(self):
        self.servo(9, 100, 100)

    def take_cup_0(self):
        self.manipulator(100, 100, 100, 100, 100, 100, 200)
        rospy.sleep(2.0)
        #self.servo(4, 1000, 1000)

    def clear_poses(self):
        for i in range (10):
            self.servo(i, 512, 200)
        
    def handleCommand(self, req):
        command = req.command
        if command == 'sosi':
            self.take_cup_0()
        elif command == 'clear':
            self.clear_poses()
        elif command == 'home':
            self.home()
        elif command == 'pretake':
            self.pretake()
        return 0

servo = TServoCommands()

servoService = rospy.Service("mb/servo/command", ServoCommand, servo.handleCommand)
servo_poses_sub = rospy.Subscriber("/servo/poses", ServoPoses, servo_poses_sub_callback)

rospy.spin()
