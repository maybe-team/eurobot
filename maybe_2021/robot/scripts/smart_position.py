#!/usr/bin/env python

import rospy
import time
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import Marker
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from std_msgs.msg import Float32, Int16
from tf.transformations import euler_from_quaternion
from math import atan2, sin, cos, hypot, pi, acos

from serial_overlay.srv import SetXY, SetTheta

roll = pitch = theta_odom = 0
ranges_from_lidar = ()
x_lidar_global = rospy.get_param('global_x')
y_lidar_global = rospy.get_param('global_y')
theta_lidar = rospy.get_param('theta')

angle_spaces = 15
dist_eps = 0.035
dangerous_zone_eps = 0.3
odom_update_v_eps = 0.25
odom_update_rate = 1
odom_update_counter = 0
theta_eps = 0.05
tag_dist_upper_bound = 3.0
odom_value = 0.5
coord_avg_max = 1

deg_to_rad = 0.01745
rad_to_deg = 57.2957

x_sum = y_sum = 0.0
x_avg = x_lidar_global
y_avg = y_lidar_global
coord_cnt = 0

odom_stack = []
odom_stack_size = 3
odom_eps = 0.02

tag_coords = [(-0.09, 1.95), (-0.09, 0.05), (3.09, 1)]

root_enable = 1

rospy.init_node('position_tag_lidar')
point_pub = rospy.Publisher('position_tag_lidar', Point, queue_size=1)
odom_update_xy_pub = rospy.Publisher('odom_update_xy', Point, queue_size=1)
odom_update_theta_pub = rospy.Publisher('odom_update_theta', Float32, queue_size=1)


def normalize_angle_rad(angle):
    while angle >= 2 * pi:
        angle -= 2 * pi
    while angle < 0:
        angle += 2 * pi
    return angle

def normalize_angle_deg(angle):
    while angle >= 360:
        angle -= 360
    while angle < 0:
        angle += 360
    return angle

def callback_lidar(msg):
    global ranges_from_lidar, theta_odom, odom_stack
    ranges_from_lidar = msg.ranges
    if len(odom_stack) < 2:
        print "no odom"
        return
    x_correct = odom_stack[-1][0]
    y_correct = odom_stack[-1][1]
    correct_position(ranges_from_lidar, x_correct, y_correct, odom_stack[-1][2])

def close_to_cup_callback(msg):
    global root_enable
    root_enable = msg.data
    #print root_enable

def correct_dist(x):
    error = -0.0189 * x ** 5 + 0.1546 * x ** 4 - 0.4487 * x ** 3 + 0.5844 * x ** 2 - 0.2799 * x + 0.032;
    return x - error + 0.05
    #return 0.9925785 * (x ** 0.9644696) + 0.05

def get_norm_tags_coords(r0, r1):
    y = (r1 ** 2 - r0 ** 2 + 3.8) / 3.8
    x = (r1 ** 2 - (y - 0.05) ** 2) ** 0.5 - 0.09
    return (x, y)

def get_amiran01(b, a):
    x = 0.5 * (((10*b - 10*a + 19)*(10*a - 10*b + 19)*(10*a + 10*b - 19)*(10*a + 10*b + 19))/36100.0) ** 0.5 - 0.09
    y = (5*a**2)/19 - (5*b ** 2)/19 + 1
    return (x, y)

def get_amiran02(a, b, outside):
    if outside:
        x = (15900*a ** 2)/110149.0 - (95 * max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5)/636.0 - (15900*b ** 2)/110149.0 + 1.5
        y = (4750*b ** 2)/110149.0 - (4750*a ** 2)/110149.0 - 0.5 * max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5 + 59/40.0
        return (x, y)
    else:
        x = (95*max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5)/636.0 + (15900*a ** 2)/110149.0 - (15900*b ** 2)/110149.0 + 1.5
        y = 0.5 * max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5 - (4750*a ** 2)/110149.0 + (4750*b ** 2)/110149.0 + 59/40.0
        return (x, y)


def get_amiran12(a, b, outside):
    if outside:
        x = (15900*a ** 2)/110149 - (95*max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5)/636.0 - (15900*b ** 2)/110149.0 + 3/2.0
        y = 0.5 * max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5 + (4750*a ** 2)/110149.0 - (4750*b ** 2)/110149.0 + 21/40.0
        return (x, y)
    else:
        x = (95*max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5)/636.0 + (15900*a ** 2)/110149.0 - (15900*b ** 2)/110149.0 + 3/2.0
        y = (4750*a ** 2)/110149.0 - 0.5 * max(0, (25281*(- 10000*a ** 2 + 20000*a*b - 10000*b ** 2 + 110149)*(10000*a ** 2 + 20000*a*b + 10000*b ** 2 - 110149))/30332005502500.0) ** 0.5 - (4750*b ** 2)/110149.0 + 21/40.0
        return (x, y)


def correct_position(ranges_from_lidar, x_odom, y_odom, theta_odom):
    global x_lidar_global, y_lidar_global, theta_lidar, tag_coords, cnt, odom_stack, odom_update_xy_pub, odom_update_theta_pub, odom_update, dangerous_zone_eps, odom_update_v_eps, theta_eps, odom_update_counter, x_sum, y_sum, coord_cnt, x_avg, y_avg, angle_spaces, dist_eps, root_enable
    x_lidar_old, y_lidar_old, theta_old = x_lidar_global, y_lidar_global, theta_lidar

    v_odom = odom_stack[-1][3]
    
    print "I get"
    print "x    ", round(x_odom, 3)
    print "y    ", round(y_odom, 3)
    print "theta", round(theta_odom * rad_to_deg)
    print "\n"
    tag_angles = [0] * 3
    
    tag_angles[0] = atan2(x_odom - tag_coords[0][0], tag_coords[0][1] - y_odom)
    tag_angles[1] = atan2(x_odom - tag_coords[1][0], y_odom - tag_coords[1][1])
    tag_angles[2] = atan2(tag_coords[2][0] - x_odom, abs(y_odom - tag_coords[2][1])) 

    tag_dists = [0] * 3
    tag_dists[0] = hypot(x_odom - tag_coords[0][0], tag_coords[0][1] - y_odom)
    tag_dists[1] = hypot(x_odom - tag_coords[1][0], y_odom - tag_coords[1][1])
    tag_dists[2] = hypot(tag_coords[2][0] - x_odom, abs(y_odom - tag_coords[2][1]))


    tag_angles[1] = pi - tag_angles[1]

    if y_odom >= 1:
        tag_angles[2] += pi
    else:
        tag_angles[2] = 2 * pi - tag_angles[2] 

    search_deg_list = [int(normalize_angle_rad(2 * pi - theta_odom + angle) * rad_to_deg) for angle in tag_angles]
    real_tags = [(0, 0, False)] * 3

    for i in range(3):
        start_angle = search_deg_list[i]
        optimal_angle = start_angle
        for angle in range(start_angle - angle_spaces, start_angle + angle_spaces):
            new_angle = normalize_angle_deg(angle)
            if abs(correct_dist(ranges_from_lidar[optimal_angle]) -  tag_dists[i]) >= abs(correct_dist(ranges_from_lidar[new_angle]) - tag_dists[i]):
                if abs(correct_dist(ranges_from_lidar[new_angle]) - tag_dists[i]) < dist_eps and correct_dist(ranges_from_lidar[new_angle]) < tag_dist_upper_bound:
                    real_tags[i] = (new_angle, correct_dist(ranges_from_lidar[new_angle]), True)
                    optimal_angle = new_angle

    real_theta = 0.0
    tags_cnt = 0
    working_list = []
    for x in real_tags:
        tags_cnt += x[2]

    huge_angle = False
    for i in range(3):
        angle = normalize_angle_deg(360 - real_tags[i][0] + tag_angles[i] * rad_to_deg)
        if angle >= 340:
            huge_angle = True

    for i in range(3):
        if real_tags[i][2]: 
            angle = normalize_angle_deg(360 - real_tags[i][0] + tag_angles[i] * rad_to_deg)
            if angle < 30 and huge_angle:
                angle += 360
            real_theta += (1.0 / tags_cnt) * angle

    real_theta = normalize_angle_deg(real_theta)

    line_02 = (-1 / 3.09) * x_odom + 1.95;
    line_12 = 1 / 3.09 * x_odom

    outside_02 = line_02 > y_odom + dangerous_zone_eps
    outside_12 = line_12 < y_odom - dangerous_zone_eps

    coords = [(0.0, 0.0)] * 3
    print "0 and 1"
    if real_tags[0][2] and real_tags[1][2]:
        print "visible"
        coords[0] = get_amiran01(real_tags[0][1], real_tags[1][1])
        working_list.append((0, True))
        print coords[0]
    else:
        """
        """
        print "unvisible"
        print "-"

    print "\n0 and 2"
    if real_tags[0][2] and real_tags[2][2]:
        print "visible"
        coords[1] = get_amiran02(real_tags[0][1], real_tags[2][1], outside_02)
        working_list.append((1, outside_02))
        print coords[1]
        if not outside_02:
            """
            """
            print "trash"
        else:
            """
            """
            print "good"
    else:
        """
        """
        print "unvisible"
        print "-"

    print "\n1 and 2"
    if real_tags[1][2] and real_tags[2][2]:
        print "visible"
        coords[2] = get_amiran12(real_tags[1][1], real_tags[2][1], outside_12)
        working_list.append((2, outside_12))
        print coords[2]
        if not outside_12:
            """
            """
            print "trash"
        else:
            """
            """
            print "good"
    else:
        """
        """
        print "unvisible"
        print "-"
   
    print "TAGS: ", real_tags[0][2], real_tags[1][2], real_tags[2][2]
    x_total = y_total = 0.0
    good_coords = False

    if len(working_list) < 1:
        angle_spaces = 20
        dist_eps = 0.07
    else:
        angle_spaces = 15
        dist_eps = 0.035

    if len(working_list) < 1:
        odom_update_counter = odom_update_rate
        x_odom, y_odom, theta_odom, v_odom = odom_stack[-2]
    elif len(working_list) == 1:
        x_total = coords[working_list[0][0]][0]
        y_total = coords[working_list[0][0]][1]
        good_coords = working_list[0][1]
    else:
        cnt_good = 0
        for i in working_list:
            if i[1]:
                cnt_good += 1
                x_total += coords[i[0]][0]
                y_total += coords[i[0]][1]

        x_total *= (1.0 / cnt_good)
        y_total *= (1.0 / cnt_good)
        good_coords = True
    
    
    print "\n"
    print "TAG 0" 
    if True or real_tags[0][2]:
        print "dist        ", real_tags[0][1]
        print "math dist   ", tag_dists[0] 
        print "local_angle ", real_tags[0][0]
        print "global_angle", tag_angles[0] * rad_to_deg
        # print "theta       ", normalize_angle_deg(360 - real_tags[0][0] + tag_angles[0] * rad_to_deg)
    print "\nTAG 1"
    if True or real_tags[1][2]:
        print "dist        ", real_tags[1][1]
        print "math dist   ", tag_dists[1]
        print "local_angle ", real_tags[1][0]
        print "global_angle", tag_angles[1] * rad_to_deg
        # print "theta       ", normalize_angle_deg(360 - real_tags[1][0] + tag_angles[1] * rad_to_deg)
    
    print "\nTAG 2"
    if True or real_tags[2][2]:
        print "dist        ", real_tags[2][1]
        print "math dist   ", tag_dists[2]
        print "local_angle ", real_tags[2][0]
        print "global_angle", tag_angles[2] * rad_to_deg
        # print "theta       ", normalize_angle_deg(360 - real_tags[2][0] + tag_angles[2] * rad_to_deg)
    
    print "\n"
    good_theta = False
    theta_odom_avg_diff = (odom_stack[-2][2] - odom_stack[-1][2])    
    if tags_cnt <= 1:
        print "dont update theta"
        theta_lidar = theta_odom * rad_to_deg
    else:
        print "tags_cnt", tags_cnt
        good_theta = True
        theta_lidar = real_theta
    """
    print "theta_odom_avg_diff", theta_odom_avg_diff
    """
    if good_coords and v_odom == 0.0 and coord_cnt < coord_avg_max:
        x_sum += x_total
        y_sum += y_total
        coord_cnt += 1
        print "sum", coord_cnt
    else:
        x_sum = x_total
        y_sum = y_total
        coord_cnt = 1
        print "dont sum", coord_cnt

    x_avg = x_sum * (1.0 / coord_cnt)
    y_avg = y_sum * (1.0 / coord_cnt)
    
    if good_coords and v_odom < odom_update_v_eps:
        print "update coords"
        x_lidar_global = (1 - odom_value) * x_avg + odom_value * x_odom
        y_lidar_global = (1 - odom_value) * y_avg + odom_value * y_odom
    elif good_coords:
        print "moving fast least update"
        coef = min(1.0, odom_value * 1.5)
        x_lidar_global = (1 - coef) * x_avg + coef * x_odom
        y_lidar_global = (1 - coef) * y_avg + coef * y_odom
    else:
        print "dont update coords (odom only)"
        x_lidar_global = x_odom
        y_lidar_global = y_odom
        x_sum = 0.0
        y_sum = 0.0
        coord_cnt = 0.0
     
    print "\n"
    
    print "avg x    ", round(x_avg, 3) 
    print "avg y    ", round(y_avg, 3)
    print "real theta ", round(real_theta)
    
    print "\n-----------\n"

    print "odom x     ", round(odom_stack[-1][0], 3)
    print "odom y     ", round(odom_stack[-1][1], 3)
    print "odom theta ", round(odom_stack[-1][2] * rad_to_deg)
    print "odom v     ", v_odom
    print "\n-----------\n"
    # print "diff x     ", round(abs(odom_stack[-1][0] - x_avg), 4)
    # print "diff y     ", round(abs(odom_stack[-1][1] - y_avg), 4)
    # print "theta diff ", round(abs(odom_stack[-1][2] * rad_to_deg - real_theta), 4)
    
    print "\nupdate odom"
    
    odom_update_counter += 1
    if good_coords and odom_update_counter >= odom_update_rate and root_enable == 0:
        odom_update_counter = 0
        print "yes"
        odom_update_counter = 0
        odom_stack.append((x_lidar_global, y_lidar_global, theta_lidar, v_odom))
        odom_update = Point()
        odom_update.x = x_lidar_global
        odom_update.y = y_lidar_global
        
        # odom_update_xy_pub.publish(odom_update)
        set_xy_proxy = rospy.ServiceProxy("serial_overlay/set_xy", SetXY)
        set_xy_proxy(x_lidar_global, y_lidar_global)
        print "KEK"
    elif good_coords and odom_update_counter >= odom_update_rate and root_enable == 1:
        print x_lidar_global, y_lidar_global
        print "LOL"
    else:
        """
        """
        print "no"
    if good_theta:
        set_theta_proxy = rospy.ServiceProxy("serial_overlay/set_theta", SetTheta)
        set_theta_proxy(theta_lidar * deg_to_rad)
        print "theta updated"
    print "\n###################\n"

def callback_odom(msg):
    global x_lidar_global, y_lidar_global, roll, pitch, ranges_from_lidar, odom_stack, odom_stack_size
    
    x_odom = msg.pose.pose.position.x
    y_odom = msg.pose.pose.position.y
    v_odom = msg.twist.twist.linear.z

    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, theta_odom) = euler_from_quaternion(orientation_list)
    theta_odom = normalize_angle_rad(theta_odom)


    odom_stack.append((x_odom, y_odom, theta_odom, v_odom))
    if len(odom_stack) > odom_stack_size:
        odom_stack = odom_stack[1:]





lidar = rospy.Subscriber('/scan', LaserScan, callback_lidar)
odom_sub = rospy.Subscriber("/odom", Odometry, callback_odom)
close_to_cup_sub = rospy.Subscriber('/close_to_cup', Int16, close_to_cup_callback)
rospy.spin()
