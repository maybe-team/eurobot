#!/usr/bin/env python

import rospy

from main_robot.msg import motors_speeds

from geometry_msgs.msg import Twist, PoseStamped

from math import cos

import sys, select, os

if os.name == 'nt':
  import msvcrt
else:
  import tty, termios




rospy.init_node('fake_teleop')
speed = motors_speeds()
control_motor = Twist()
deg_to_rad = 0.017453292519943
rad_to_deg = 57.29577951308233


robot_pos = PoseStamped()

robot_pos.pose.position.x = 0.0
robot_pos.pose.position.y = 0.0

fake_robot = rospy.Publisher("/aruco/robot4", PoseStamped, queue_size = 1)

def getKey():
    if os.name == 'nt':
        return msvcrt.getch()
    else:
        settings = termios.tcgetattr(sys.stdin)
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 10)
    
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''
     
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    
    return key

while not rospy.is_shutdown():
    key = getKey()
    
    dx, dy = 0, 0

    if key == '':
        pass 
    elif key == 'w' :
        dx = 0.01
        dy = 0.0
    elif key == 'x' :
        dx = -0.01
        dy = 0.0
    elif key == 'a' :
        dx = 0.0
        dy = -0.01
    elif key == 'd' :
        dx = 0.0
        dy = 0.01 
    elif key == 's' :
        dx = 0.0
        dy = 0.0
    else:
        if (key == '\x03'):
            break
    
    robot_pos.pose.position.x += dy
    robot_pos.pose.position.y += dx

    fake_robot.publish(robot_pos)
