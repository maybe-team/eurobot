#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import Marker
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from sensor_msgs.msg import Range
from math import atan2, sin, cos
ranges_from_lidar = ()
ranges_transform = [0]*360
scan_transform = LaserScan()
x = 0.0000001
y = 0.0000001
roll = pitch = phi = 0.0
deg_to_rad = 0.01745
rad_to_deg = 57.2957

rospy.init_node('lidar_transform')
publisher1 = rospy.Publisher('marker1', Marker, queue_size=1)
ir2_pub = rospy.Publisher('ir2', Range, queue_size=1)
pub = rospy.Publisher('/scan_transform', LaserScan, queue_size=1)

def callback_lidar(msg):
    global ranges_from_lidar, scan_transform, deg_to_rad,x,y, phi
    ranges_from_lidar = msg.ranges
    scan_transform = msg

def callback_odom (msg):
    global x, y, roll, pitch, phi
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, phi) = euler_from_quaternion (orientation_list)
    phi = int(phi*rad_to_deg)
    if phi < 0:
        phi = phi + 360
def transform():
    global phi, x, y, ranges_transform, ranges_from_lidar, deg_to_rad, rad_to_deg, scan_transform

    angle1 = int((atan2(x, (2 - y))) * rad_to_deg)
    angle2 = 90
    angle3 = int((atan2(y, x)) * rad_to_deg) + 90
    angle4 = 180
    angle5 = int((atan2((3 - x), y)) * rad_to_deg) + 180
    angle6 = 270
    angle7 = int((atan2((2 - y), (3 - x))) * rad_to_deg) + 270
    angle8 = 360

    i_count = 0
    i = phi
    if i >= 360:
        i -= 360

    while i_count < 360:
        if ranges_from_lidar[i_count] == 0.032999999821186066:
            current = 4.5
        else:
            current = ranges_from_lidar[i_count]
        if 0 <= i < angle1:
            if current < (2 - y) / cos(i * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0

        elif angle1 <= i < angle2:
            if current < x / cos((90 - i) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] =  0

        elif angle2 <= i < angle3:
            if current < x / cos((i - 90) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0

        elif angle3 <= i < angle4:
            if current < y / cos((180 - i) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0

        elif angle4 <= i < angle5:
            if current < y / cos((i - 180) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0

        elif angle5 <= i < angle6:
            if current < (3 - x) / cos((270 - i) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0

        elif angle6 <= i < angle7:
            if current < (3 - x) / cos((i - 270) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0

        elif angle7 <= i < angle8:
            if current < (2 - y) / cos((360 - i) * deg_to_rad):
                ranges_transform[i_count] = current
            else:
                ranges_transform[i_count] = 0
        if i >= 359:
            i = 0
        else:
            i += 1

        i_count+= 1

    scan_transform.header.frame_id = 'laser_link'
    scan_transform.ranges = ranges_transform
    pub.publish(scan_transform)

lidar = rospy.Subscriber('/scan', LaserScan, callback_lidar)
odom_sub = rospy.Subscriber("/odom", Odometry, callback_odom)
rospy.sleep(2.)
r = rospy.Rate(5)
while not rospy.is_shutdown():
    transform()

    r.sleep()
