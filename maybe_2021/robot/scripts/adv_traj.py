#!/usr/bin/env python

import rospy

import datetime, threading

from math import atan2, cos, sin

from maybe_lib.MaybePID import TPIDRegulator

from serial_overlay.srv import SetSpeed, SendVelVector

from maybe_lib.MaybeMath import TPoint2D, TVector2D, TSegment
from maybe_lib.MaybeMath import vectorFromPoints, vectorProd, vectorPseudo, distFromSegment, ortVector
from maybe_lib.MaybeMath import degToRad, radToDeg, normalizeAngle, quaternionToAngle

from maybe_lib.MaybePID import TPIDRegulator

from maybe_lib.MaybeEntity import TRobot

from maybe_lib.srv import BaseCommand, VelocitySettings, TrajectoryPath, MoveTheta, MovePathResponse

from main_robot.msg import motors_speeds

from nav_msgs.msg import Path, Odometry

from geometry_msgs.msg import PoseStamped

rospy.init_node("traj")


"""
Makes functional time -> velocity
"""
# TODO: make new functional that uses also real (odom) velocity
class TDynamicFunctionFactory:
    def __init__(self):
        pass
    
    """
    generates function THAT
    changes velocity from v1 to v2 (v1 > v2 or v2 > v1 DOES NOT MATTER) by time t
    a is coeff of x^2 in quadratic equation
    Bigger a - faster dynamics
    Lower  a - smooth dynamics
    """
    def generate(self, v1, v2, t, a):
        b = (v2 - v1 - a * t * t) / t 
        def func(x):
            if x < 0:
                return v1
            elif x > t:
                return v2
            res = a * x ** 2 + b * x + v1
            return max(min(v1, v2), min(res, max(v1, v2)))
        return func

class TTrajectoryRegulator:
    def __init__(self):
        self.lock = threading.Lock() 

        self.path = Path()
        self.pathChanged = False
        self.robot = TRobot()
        odom_sub = rospy.Subscriber("odom", Odometry, self.odomCallback)
        self.motors = rospy.Publisher("motor_power", motors_speeds, queue_size = 1)
        
        self.drawnPath = Path()
        self.drawPathPub = rospy.Publisher("/realpath", Path, queue_size = 1) 
        
        """
        baseV    - max velocity
        minV     - min velocity
        velocity - velocity that we WANT (not real velocity)
        """
        self.baseV = 0.5
        self.minV = 0.1
        self.velocity = self.minV
        
        """
        fact     -  makes new trajFunc with new params
        trajFunc -  takes time && return new velocity that we WANT
        """
        self.fact = TDynamicFunctionFactory()
        self.trajFunc = None
        
        """
        trajectoryPID - forces robot to move with respect to path
        """
        #self.errorPID = TPIDRegulator(pK = 10, iK = 0.0, dK = 0.2, maxOutput = 1.0, minOutput = -1.0, maxSumError = 3.0, minSumError = -3.0)
        
        self.errorPID = TPIDRegulator(pK = 10, iK = 0.0, dK = 0.2, maxOutput = 1.0, minOutput = -1.0, maxSumError = 3.0, minSumError = -3.0)
        """
        sidePID       - forces robot to move with theta
        """
        self.sidePID = TPIDRegulator(pK = 0.6, iK = 0.065, dK = 0.005, maxOutput = 1.5, minOutput = -1.5, maxSumError = 1, minSumError = -1, dF = self.angleDiff)
        
        """
        theta    - angle that we WANT
        """
        self.theta = degToRad(270)
        self.sidePID.setTarget(self.theta)

        self.enable = True
        self.abort  = False

    def motorsPub(self, speed, angle, sideSpeed):
        speedProxy = rospy.ServiceProxy("serial_overlay/set_speed", SetSpeed)
        vx = speed * cos(normalizeAngle(angle + degToRad(90)))
        vy = speed * sin(normalizeAngle(angle + degToRad(90)))
        wz = -sideSpeed
        speedProxy(vx, vy, wz)
    
    def odomCallback(self, msg):
        self.robot.x = msg.pose.pose.position.x
        self.robot.y = msg.pose.pose.position.y
        self.robot.orient = msg.pose.pose.orientation
        (_, _, self.robot.theta) = quaternionToAngle([self.robot.orient.x, self.robot.orient.y, self.robot.orient.z, self.robot.orient.w])
        self.robot.theta = normalizeAngle(self.robot.theta)
    
    def handleVector(self, req):
        vectorProxy = rospy.ServiceProxy("serial_overlay/set_speed", SetSpeed)
        accuracy = 0.07
        while ((abs(req.finishX - self.robot.x) > accuracy or abs(req.finishY - self.robot.y) > accuracy) and self.enable):
            #print(req.vel)
            #print(req.move_angle)
            vx = req.vel * (cos(degToRad(req.move_angle)))
            vy = req.vel * (sin(degToRad(req.move_angle)))
            #print(vx)
            #print(vy)
            res = vectorProxy(vx, vy, 0)
            print(abs(req.finishX - self.robot.x))  
            print(abs(req.finishY - self.robot.y))  
            print("*****")  
        vectorProxy(0, 0, 0)
        print("END")
        return 0
    
    def handleTheta(self, req):
        self.theta = req.theta
        print "got theta", self.theta
        self.sidePID.setTarget(self.theta)
        self.sidePID.resetAll() 
        return 0

    def handlePath(self, req):
        print "got path"
        print req.plan
        # with self.lock:
        self.path = req.plan
        self.pathChanged = True
        return 0
    
    def handleVelocity(self, req):
        with self.lock:
            self.baseV    = req.base
            self.minV     = req.min  
            self.baseTime = datetime.datetime.now()
            self.trajFunc = self.fact.generate(v1 = max(self.minV, self.velocity), v2 = self.baseV, t = 1.0, a = 0.0)
            print "v changed", self.baseV, self.velocity, self.minV
        return 0
    
    """
    Gets current position of robot and RETURNS INDEX OF PATH. In other words finds place in path where robot is
    """
    def pointToIndex(self, point):
        minDst = 10 ** 9
        index = 0
        for i in range(len(self.path.poses) - 1):
            A = self.path.poses[i].pose.position
            A = TPoint2D(A.x, A.y)
            B = self.path.poses[i + 1].pose.position
            B = TPoint2D(B.x, B.y)
            C = point
            """
            [A, B] - current segment
            C - robot position
            """
            segment = TSegment(A, B)
            dst = distFromSegment(segment, C)
            if dst < minDst:
                minDst = dst
                index = i
        """
        index - point of path, such that
        [path[index], path[index + 1]] is the closest segment to poin (with dist = minDst)
        """
        return index, minDst
    """
    Takes vector and returns angle that will be sent on motors 
    """
    def calcAngle(self, vec):
        return normalizeAngle(degToRad(360.0) - atan2(vec.x, vec.y) - self.robot.theta)
    
    """
    Calc minimal angle difference. Used in sidePID
    """
    def angleDiff(self, theta, aim): 
        if normalizeAngle(aim - theta) > normalizeAngle(theta - aim):
            return -normalizeAngle(theta - aim)
        else:
            return normalizeAngle(aim - theta)
   
    def handleEnable(self, req):
        print "enable", req
        self.enable = (req.command == 1)
        return 0
    
    def handleAbort(self, req):
        print "abord", req
        self.abort = (req.command == 1)
        return 0
    
    def movePath(self, req): 
        if True:

            """
            First - prepare trajectory to move
            """
            
            """
            cosCoef - forces robot to move slow on turns. See below
            """
            cosCoef = 2
            

            """
            If there are no coords - returns
            """
            (x, y, theta) = (self.robot.x, self.robot.y, self.robot.theta)
            if x is None or y is None or theta is None:
                return 1
            
            cur = TPoint2D(x, y)
            index, dst = -1, 10 ** 9
            counter = 0
            angle = 0.0
 
            start = TPoint2D(self.path.poses[0].pose.position.x, self.path.poses[0].pose.position.y)
            finish = TPoint2D(self.path.poses[-1].pose.position.x, self.path.poses[-1].pose.position.y)
            
            totalDist = vectorFromPoints(start, finish).length()

            start, aim = None, None

            inc = True

            self.baseTime = datetime.datetime.now()
            now = None
            
            """
            clear trajectory PID
            """
            self.errorPID.setTarget(0.0) 
            self.errorPID.resetAll() 
            
            """
            make veclocity function
            """
            self.trajFunc = self.fact.generate(v1 = self.minV, v2 = self.baseV, t = 1, a = 0)    
        
            while index + 1 < len(self.path.poses) and counter < 10 ** 9:
                """
                Handle enable and abort
                """
                if not self.enable:
                    rospy.sleep(0.01)
                    self.velocity = 0.0
                    self.motorsPub(self.velocity, angle, 0.0)
                    continue
                if self.abort:
                    self.velocity = 0.0
                    self.motorsPub(self.velocity, angle, 0.0)
                    self.abort = False
                    return 1
                
                """
                Now all (almost) data is fixed
                """
                with self.lock:
                    (x, y, theta) = (self.robot.x, self.robot.y, self.robot.theta) 
                
                    cur = TPoint2D(x, y)
                    index2, dst = self.pointToIndex(cur) 
                
                    realCur = PoseStamped()
                    realCur.pose.position.x = x
                    realCur.pose.position.y = y
                    
                    """
                    Draw real trajectory with rviz
                    """
                    self.drawnPath.header.frame_id = "map"

                    self.drawnPath.poses.append(realCur)
                    self.drawPathPub.publish(self.drawnPath)
                    
                    """
                    Check wether we passed previous goal (should we change current start and goal?)
                    """
                    if index2 != index or self.pathChanged:
                        if not index2 < len(self.path.poses):
                            print "we are in the end lol"
                            break

                        self.pathChanged = False
                        baseTime = datetime.datetime.now()
                        print "change points"
                        inc = True
                        start = self.path.poses[index2].pose.position
                        start = TPoint2D(start.x, start.y)
                        aim = self.path.poses[index2 + 1].pose.position
                        aim = TPoint2D(aim.x, aim.y)
                        self.trajFunc = self.fact.generate(v1 = max(self.minV, self.velocity), v2 = self.baseV, t = 0.5, a = 0)
                
                    index, index2 = index2, index
                
                    curToAim   = vectorFromPoints(cur, aim)
                    startToAim = vectorFromPoints(start, aim)
                    
                    """
                    Calc trajectory error SIGN (left or right) from area - will be used in errorPID
                    """
                    area  = vectorPseudo(curToAim, startToAim)
                    errorSign = -cmp(0, area)
                    
                    """
                    ortStartToAim - vector that is orthogonal to current trajectory vector
                    We need it to return robot on trajectory
                    """
                    ortStartToAim = ortVector(startToAim) * errorSign
                    distToAim  = curToAim.length()
                    segDist    = startToAim.length()

                    progress = max(0, (distToAim ** 2 - dst ** 2)) ** 0.5

                    distToFinish = vectorFromPoints(cur, finish).length()
                    
                    """
                    Check if we have to slow down (are we near end?)
                    """
                    if inc and (progress <= 0.25 or distToFinish <= 0.35):
                        inc = False
                        baseTime = datetime.datetime.now() 
                        
                        #TODO: add call to global planner here

                        """
                        We are going to calculate cos of turn angle. If angle is bad - cos is bigger
                        cosCoef - pow; new velocity will be 
                        v = baseV * |cos(a)| ^ cosCoef
                        Bigger cosCoef - less slowdown
                        Lower  cosCoef - more slowdown
                        """
                        if index + 2 == len(self.path.poses) or distToFinish <= 1.0: 
                            slowCos = 0.0
                        else:  
                            vector1 = vectorFromPoints(start, aim).normalize()
                            
                            nextAim = self.path.poses[index + 2].pose.position
                            nextAim = TPoint2D(nextAim.x, nextAim.y)
                        
                            vector2 = vectorFromPoints(aim, nextAim).normalize()
                            
                            """
                            vector1 - current trajectory vector
                            vector2 - next trajectory vector
                            """

                            slowCos = 0.0
                            
                            """
                            More cos - less velocity
                            """
                            if vector1.length() * vector2.length() != 0.0:
                                slowCos = vectorProd(vector1, vector2) / (vector1.length() * vector2.length())
                                slowCos = abs(slowCos) ** cosCoef

                        print "slow down"
                        """
                        Generating new velocity function with respect for previous decision
                        """
                        self.trajFunc = self.fact.generate(v1 = max(self.minV, self.velocity), v2 = max(self.minV, slowCos * self.baseV), t = 0.5, a = -0.025)
                        progress = 0.0
                    
                    """
                    errorPID tells us PROPORTION of trajectory correction.
                    If corrCoef = 0 - we don't correct trajectory at all
                    IF corrCoef = 1 - we don't move by trajectory, but returning on it instead
                    """
                    corrCoef = abs(self.errorPID.process(dst * errorSign))
                    
                    """
                    trajVector - actual vector
                    """
                    trajVector = (startToAim.normalize() * (1.0 - corrCoef) + ortStartToAim.normalize() * corrCoef) * 0.5
                
                    angle = self.calcAngle(trajVector)
                
                    now = datetime.datetime.now()
                    deltaTime = (now - baseTime).seconds + ((now - baseTime).microseconds / (10.0 ** 6.0))
                    """
                    HERE we use our generated functions
                    """
                    self.velocity = self.trajFunc(deltaTime)
                    toFinish = vectorFromPoints(cur, finish) 
                    # print self.velocity
                    # print toFinish.length()
                    # print "==="
                    
                    """
                    take sideSpeed from PID
                    """
                    sideSpeed = self.sidePID.process(theta)
                                  
                    """
                    Check finish point (stops robot)
                    """
                    if toFinish.length() < 0.075:
                        # print cur.x, cur.y
                        # print aim.x, aim.y
                        angle = self.calcAngle(toFinish)
                        if toFinish.length() < 0.01:
                            break 
                    self.motorsPub(self.velocity, angle, sideSpeed)
   
                    rospy.sleep(0.01)
                    counter += 1

            self.motorsPub(0.0, angle, 0.0)

        return 0

traj = TTrajectoryRegulator()

sendPath  = rospy.Service("/mb/trajectory/send_path",  TrajectoryPath, traj.handlePath)
movePath  = rospy.Service("/mb/trajectory/move_path",  BaseCommand, traj.movePath)
velocity  = rospy.Service("/mb/trajectory/velocity",   VelocitySettings, traj.handleVelocity)
enable    = rospy.Service("/mb/trajectory/enable",     BaseCommand, traj.handleEnable)
abort     = rospy.Service("/mb/trajectory/abort",      BaseCommand, traj.handleAbort)
theta     = rospy.Service("/mb/trajectory/send_theta", MoveTheta,   traj.handleTheta)
vector    = rospy.Service("/custom/send_vel_vector", SendVelVector, traj.handleVector)
rospy.spin()
