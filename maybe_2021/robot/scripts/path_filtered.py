#!/usr/bin/env python
import rospy

from nav_msgs.msg import Path, Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PoseStamped

path = Path()
path.header.frame_id = 'odom'

rospy.init_node('Path_filtered')
pub = rospy.Publisher("path_filtered", Path, queue_size=10)

def callback_odom (msg):
    global path
    pose = PoseStamped()
    pose.header = msg.header
    pose.pose = msg.pose.pose
    path.poses.append(pose)
    pub.publish(path)


odom_sub = rospy.Subscriber("/odometry/filtered", Odometry, callback_odom)
rospy.spin()
