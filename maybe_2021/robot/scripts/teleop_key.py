#!/usr/bin/env python

import rospy
from serial_overlay.srv import SetSpeed
from geometry_msgs.msg import Twist
from math import cos
import sys, select, os
if os.name == 'nt':
  import msvcrt
else:
  import tty, termios



speed_proxy = rospy.ServiceProxy("/serial_overlay/set_speed", SetSpeed)
rospy.init_node('main_robot_teleop')
control_motor = Twist()
deg_to_rad = 0.017453292519943
rad_to_deg = 57.29577951308233

def getKey():
    if os.name == 'nt':
        return msvcrt.getch()
    else:
        settings = termios.tcgetattr(sys.stdin)
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 10)
    
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''
     
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    
    return key

while not rospy.is_shutdown():
    key = getKey()
    if key == '' :
        pass
    elif key == 'w' :
        control_motor.linear.y += 0.02
    
    elif key == 'x' :
        control_motor.linear.y -= 0.02 

    elif key == 'a' :
        control_motor.linear.x -= 0.02 

    elif key == 'd' :
       control_motor.linear.x += 0.02    

    elif key == 'q' :
        control_motor.angular.z += 0.25 

    elif key == 'e' :
        control_motor.angular.z -= 0.25 

    elif key == 's' :
        control_motor.linear.x = 0
        control_motor.linear.y = 0
        control_motor.angular.z = 0
        


    else:
            if (key == '\x03'):
                break

    speed_proxy(control_motor.linear.x, control_motor.linear.y, control_motor.angular.z)
