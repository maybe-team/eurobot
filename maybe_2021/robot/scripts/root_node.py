#!/usr/bin/env python

import rospy

from math import atan2, asin, acos, sin, cos

from maybe_lib.MaybeScheduler import ITaskNode, TScheduler

from maybe_lib.MaybeMath import TPoint2D, TVector2D, TSegment, vectorFromPoints, distFromSegment
from maybe_lib.MaybeMath import radToDeg, degToRad, quaternionToAngle, normalizeAngle

from maybe_lib.MaybeUtil import waitForPublisher, debug, TRuntimeContext
from maybe_lib.MaybeEntity import TRobot

from std_msgs.msg import String, Int16
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import PoseStamped, Point

from maybe_lib.srv import BaseCommand, VelocitySettings, LocalPath
from maybe_lib.srv import CheckPath, TrajectoryPath, MoveTheta, ServoCommand
from cost_map_server.srv import RemoveCupWithNumber
from serial_overlay.srv import SendVelVector

from maybe_lib.msg import MaybeCupList

from global_planner.srv import PathPlan

infRad = 0.7
output = open("/home/odroid/catkin_ws/src/maybe_2021/robot2/output/root_node.txt", "w")


rospy.init_node('root_node')

"""
runCons - run array of functions step by step; execution stops when one of functions returns not 0

functionsWithArguments: [(0: function, 1: args, 2: kwargs)]

result: int
"""
def runCons(functionsWithArguments):
    for x in functionsWithArguments:
        res = x[0](*x[1], **x[2])
        print "done one"
        if res != 0:
            return res
    return 0

"""
runWithController - run two functions at the same time. Fist function called core - does job. Second fucntiob called ctrl - controles core and stops it (make decision by itself)

coreF:      core function
coreArgs:   list of coreF arguments
coreKwArgs: dict of coreF key arguments

result:     None
"""
def runWithController(coreF, coreArgs, coreKwArgs, ctrlF, ctrlArgs, ctrlKwArgs):
    tasks = []

    ctxData = dict()
    ctxData["scheduler"] = {"debug": False, "rootRate": 0.5, "rootControlEnabled": False}
    ctxData["reg"] = dict()
    ctxData["reg"]["ready"] = False

    ctx = TRuntimeContext(ctxData)

    print coreArgs

    tasks.append(TExecWithCtx("core", [], coreF, coreArgs, coreKwArgs, ctx))
    tasks.append(TExecWithCtx("ctrl", [], ctrlF, ctrlArgs, ctrlKwArgs, ctx)) 

    sched = TScheduler(ctx)

    for x in tasks:
        sched.addTask(x)
    
    sched.build()
    sched.start()

"""
globalPlanne - send (start, goal) to global planner node, returns result, publishes path to rviz
start:  (float, float)
goal:   (float, float)

return: res -> nav_msgs/Path 
"""
def globalPlanner(start, goal):
    
    proxy = rospy.ServiceProxy("mb/planner/global", PathPlan)
        
    startReq = PoseStamped()
    goalReq  = PoseStamped()
        
    startReq.pose.position.x = start[0]
    startReq.pose.position.y = start[1]
        
    goalReq.pose.position.x = goal[0]
    goalReq.pose.position.y = goal[1]
         
    res = proxy(startReq, goalReq)
    path = Path()
    pub = rospy.Publisher("planner/gp/path_simple", Path, queue_size = 1) 
    pub.publish(res.result)
    return res

"""
sendThetaToTrajectory - send theta to trajectory node. IT DOES NOT CAUSE MOVING. Just move theta to trajectory node

theta: float \in [0; 2 * pi)

return res -> int
"""
def sendThetaToTrajectory(theta):
    proxy = rospy.ServiceProxy("mb/trajectory/send_theta", MoveTheta)
    res = proxy(theta)
    return res

"""
sendPathToTrajectory - send path to trajectory node. IT DOES NOT CAUSE MOVING. Just move path to trajectory node

path:  nav_msgs/Path

return res -> int 
"""
def sendPathToTrajectory(path): 
    proxy = rospy.ServiceProxy("mb/trajectory/send_path",  TrajectoryPath)
    res = proxy(path)
    return res

"""
moveTrajectory - send COMMAND to trajectory node. IT DOES CAUSE MOVING. 
mode:   int (1 - move, others are not defined yet)

return: res -> int
"""
def moveTrajectory(mode):
    proxy = rospy.ServiceProxy("mb/trajectory/move_path", BaseCommand)
    res = proxy(mode)
    return res

"""
velocityTrajectory - send velocity settings to trajectory node
baseV:   something like maximal velocity
minV:    minimal velocity

return: res -> int
"""
def velocityTrajectory(baseV, minV):
    proxy = rospy.ServiceProxy("mb/trajectory/velocity", VelocitySettings)
    res = proxy(baseV, minV)
    return res

def removeCupWithNum(num):
    proxy = rospy.ServiceProxy("cost_map_server/remove_cup_with_number", RemoveCupWithNumber)
    res = proxy(num)
    print "executed"
    return res


"""
servoCommand - send command to servo node
command: string
return:  res -> int
"""
def servoCommand(command):
    servoProxy = rospy.ServiceProxy("mb/servo/command", ServoCommand)
    res = servoProxy(command)
    return res

"""
localPlanner - send path to local planner node. Returns corrected path
path:   nav_msgs/Path

return: result -> nav_msgs/Path, length -> float, status -> int
"""
def localPlanner(path):
    plannerProxy = rospy.ServiceProxy("mb/planner/local", LocalPath)
    res = plannerProxy(path) 
    return res

"""
chackPath - send path to local planner node. Return 1 iff path is clear, 0 otherwise
path:    nav_msgs/Path
return:  ok -> int
"""
def checkPath(path):
    plannerProxy = rospy.ServiceProxy("mb/planner/local/check", CheckPath)
    res = plannerProxy(path) 
    return res

"""
enableTraj - send enable command to trajectory node. If enable is True - moving is enabled, if enable is False - moving is disabled BUT BOT STOPPED (if you want to - see abortTraj)
enable: bool
return: res -> int
"""
def enableTraj(enable):
    enableProxy = rospy.ServiceProxy("mb/trajectory/enable", BaseCommand)
    res = enableProxy(enable)
    return res
"""
abortTraj - send abort command to trajectory node. If abort is True - moving task WILL BE STOPPED
abort: bool
return: res -> int
"""
def abortTraj(abort):
    abortProxy  = rospy.ServiceProxy("mb/trajectory/abort", BaseCommand)
    res = abortProxy(abort)
    return res

"""
[Scheduler task] executes f with arguments
f: function
args:   list of arguments
kwArgs: dist of named arguments
ctx:    context
"""
class TExecWithCtx(ITaskNode):
    def __init__(self, name, symbDeps, f, args, kwArgs, ctx):
        ITaskNode.__init__(self, name, symbDeps)
        self.f      = f
        self.args   = args
        self.kwArgs = kwArgs
        self.ctx    = ctx
    
    def task(self):
        self.f(self.ctx, *(self.args), **(self.kwArgs))
"""
[Scheduler task] waits for t seconds
t: float
"""
class TWaitTask(ITaskNode):
    def __init__(self, name, symbDeps, t):
        ITaskNode.__init__(self, name, symbDeps)
        self.t = t
    
    def task(self):
        rospy.sleep(self.t)

"""
[Scheduler task] executes globalPlanner
moves result to ctx.data["reg"]["global_path"] and ctx.data["reg"]["actual_path"]
start: (float, float)
goal:  (float, float)
"""
class TGlobalPlannerClient(ITaskNode):
    def __init__(self, name, symbDeps, plan):
        ITaskNode.__init__(self, name, symbDeps)
        self.plan = plan
    def task(self):
        print "global planner task"
        start = (self.plan.startX, self.plan.startY)
        goal = (self.plan.goalX, self.plan.goalY)
        #print start
        #print goal
        if (self.plan.planId in self.ctx.data["reg"]["node"].cachedPaths):
            result = self.ctx.data["reg"]["node"].cachedPaths[self.plan.planId]
        else:
            result = globalPlanner(start, goal)
        #print result.result
        self.ctx.data["reg"]["global_path"] = result.result
        self.ctx.data["reg"]["actual_path"] = self.ctx.data["reg"]["global_path"]

class TPathPlan:
    def __init__(self, startX, startY, goalX, goalY, planId):
        self.startX = startX
        self.startY = startY
        self.goalX = goalX
        self.goalY = goalY
        self.planId = planId

class TGlobalPlannerUpdateCacheNext(ITaskNode):
    def __init__(self, name, symbDeps):
        ITaskNode.__init__(self, name, symbDeps)
        self.plan = None
    def task(self):
        print 'TGlobalPlannerUpdateCacheNext'
        curPlan = self.ctx.data["reg"]["node"].curPathPlan
        if curPlan >= len(self.ctx.data["reg"]["node"].pathPlan):
            return 1
        self.plan = self.ctx.data["reg"]["node"].pathPlan[curPlan]
        start = (self.plan.startX, self.plan.startY)
        goal = (self.plan.goalX, self.plan.goalY)
        result = globalPlanner(start, goal)
        #print result.result
        self.ctx.data["reg"]["node"].cachedPaths[self.plan.planId] = result
        self.ctx.data["reg"]["node"].curPathPlan += 1

"""
[Scheduler task] executes sendPathToTrajectory
takes path from ctx.data["reg"]["actual_path"]
"""
class TPathToTrajectory(ITaskNode): 
    def __init__(self, name, symbDeps):
        ITaskNode.__init__(self, name, symbDeps)

    def task(self):
        print "path to trajectory task"
        path = self.ctx.data["reg"]["actual_path"]
        """
        robot = self.ctx.data["reg"]["node"].robot
        print robot
        path.poses.pose.position.x = robot.x
        path.poses.pose.position.y = robot.y
        """
        print 'before sending path'
        sendPathToTrajectory(path)
        print 'path was sent'

"""
[Scheduler task] executes removeCupWithNum
cupId: int
"""
class TRemoveCup(ITaskNode):
    def __init__(self, name, symbDeps, cupId):
        ITaskNode.__init__(self, name, symbDeps)
        self.cupId = cupId

    def task(self):
        print "Remove cup task"
        result = removeCupWithNum(self.cupId)
        print "Removed cup", self.cupId

"""
[Scheduler task] executes sendThetaToTrajectory
theta: float
"""
class TThetaToTrajectory(ITaskNode):
    def __init__(self, name, symbDeps, theta):
        ITaskNode.__init__(self, name, symbDeps)
        self.theta = theta

    def task(self):
        sendThetaToTrajectory(self.theta)
"""
[Scheduler task] executes moveTrajectory
"""
class TMoveToTrajectory(ITaskNode):
    def __init__(self, name, symbDeps):
        ITaskNode.__init__(self, name, symbDeps)

    def task(self):
        print "move task"
        self.ctx.data["reg"]["moving"] = True
        enableTraj(True)
        
        res = moveTrajectory(0)
        
        if res.res != 0:
            self.ctx.data["reg"]["status"] = 1
        self.ctx.data["reg"]["moving"] = False
"""
[Scheduler task] executes velocityTrajectory
baseV: float
minV : float
"""
class TVChange(ITaskNode):
    def __init__(self, name, symbDeps, baseV, minV):
        ITaskNode.__init__(self, name, symbDeps)
        self.baseV = baseV
        self.minV  = minV
    
    def task(self):
        print 'vel'
        velocityTrajectory(baseV = self.baseV, minV = self.minV)  

"""
[Scheduler task] executes servoCommand
command: string
"""
class TServoClient(ITaskNode):
    def __init__(self, name, symbDeps, command):
        ITaskNode.__init__(self, name, symbDeps)
        self.command = command
    
    def task(self):
        servoCommand(self.command) 

"""
[Scheduler task] runs local planner task, wich corrects path depending on opponent`s robots
Starts when ctx.data["reg"]["moving"] became True
Stops  when ctx.data["reg"]["moving"] became False or ctx.data["reg"]["status"] became 0
Works only when minDist is small (now it is calculated only for one enemy`s robot). It is not hard to extend it to 2 robots
"""
class TLocalPlannerClient(ITaskNode):
    def __init__(self, name, symbDeps):
        ITaskNode.__init__(self, name, symbDeps)
    def task(self):
        while not self.ctx.data["reg"]["moving"]: 
            rospy.sleep(0.1)
        
        free = True
        slow = False
        enabled = True
        
        while self.ctx.data["reg"]["moving"] and self.ctx.data["reg"]["status"] == 0:
            selfRaw   = self.ctx.data["reg"]["self"]
            selfPoint = TPoint2D(selfRaw.x, selfRaw.y)
            
            enemyRaw  = self.ctx.data["reg"]["enemy"]
            enemyPoint = TPoint2D(enemyRaw.x, enemyRaw.y)

            actualPath = self.ctx.data["reg"]["actual_path"]
            
            """
            Find minimal distance from ACTUAL (CORRECTED) path to enemy
            """
            # TODO: add second enemy

            minDist = 10 ** 9
            
            for i in range(1, len(actualPath.poses)):
                A = actualPath.poses[i - 1].pose.position
                B = actualPath.poses[i].pose.position

                A = TPoint2D(A.x, A.y)
                B = TPoint2D(B.x, B.y)

                C = enemyPoint

                segment = TSegment(A, B)
                dst     = distFromSegment(segment, C)

                if dst < minDist:
                    minDist = dst

            """
            Find minimal distance from ORIGINAL (GLOBAL) path to enemy
            """
            globalPath = self.ctx.data["reg"]["global_path"]
            minDistGlobal = 10 ** 9
            
            for i in range(1, len(globalPath.poses)):
                A = globalPath.poses[i - 1].pose.position
                B = globalPath.poses[i].pose.position

                A = TPoint2D(A.x, A.y)
                B = TPoint2D(B.x, B.y)

                C = enemyPoint

                segment = TSegment(A, B)
                dst     = distFromSegment(segment, C)

                if dst < minDistGlobal:
                    minDistGlobal = dst
            
            """
            If actual minimal distance is lower than some constant - use local planner to evade
            """
            if free and minDist <= 0.45:
                print "Evading!"
                free = False
                velocityTrajectory(minV = 0.1, baseV = 0.2)
                result = localPlanner(self.ctx.data["reg"]["actual_path"])
                """
                Check wether new path is valid
                """
                # TODO: status is not so good. Costmap and global planner require some changes
                # TODO: use it as it is. I will fix it some day

                if result.status != 0:
                    print "Bad Path"
                else:
                    print "Good Path"
                    self.ctx.data["reg"]["actual_path"] = result.result
                    sendPathToTrajectory(result.result)
            elif not free and minDistGlobal > 0.45:
                self.ctx.data["reg"]["actual_path"] = self.ctx.data["reg"]["global_path"]
                velocityTrajectory(minV = 0.1, baseV = 0.25)
                free = True
            elif not free and minDist <= 0.45 and selfToRobot.length() <= 0.3:
                print "DOUBLE!!!!"
                velocityTrajectory(minV = 0.1, baseV = 0.2)
                result = localPlanner(self.ctx.data["reg"]["actual_path"])
                if result.status != 0:
                    print "Bad Path"
                else:
                    print "Good Path"
                    self.ctx.data["reg"]["actual_path"] = result.result
                    sendPathToTrajectory(result.result)
            
            selfToRobot = vectorFromPoints(enemyPoint, selfPoint)
            
            """
            If we are too close to enemy - send enable = False command to trajectory. (Stop moving but still trying)
            """
            if enabled and minDist <= 0.2 and selfToRobot.length() <= 0.3:
                enabled = False 
                enableTraj(False)
                print "DISABLE", minDist
            
            """
            If it is not so dangerous - send enable = True command to trajectory. (Start moving)
            """
            if not enabled and (minDist > 0.2 or selfToRobot.length() > 0.3):
                enabled = True
                enableTraj(True)
            
            # TODO: make velocity parameters instead of numeric constants
            """
            If we are close to enemy - send new velocity settings to trajectory node (Slow down)
            """
            if not slow and selfToRobot.length() < 0.45:
                print "Send slow!"
                velocityTrajectory(minV = 0.1, baseV = 0.1)
                slow = True
            
            """
            If itis not so dangerous - send default velocity settings to trajectory node
            """
            if slow and selfToRobot.length()  >= 0.45: 
                velocityTrajectory(minV = 0.1, baseV = 0.2)
                slow = False

        print "LOCAL PLANNER FINISHED"
"""
Main object class
"""

class TRootNode:
    def __init__(self):
        self.robot    = TRobot()
        self.enemy    = TRobot()
        self.rawCups  = MaybeCupList()
        
        self.cachedPaths = dict()
        self.pathPlan = []
        self.curPathPlan = 0

        self.steps = [TPoint2D(0.24, 1.3)]
        self.root = []
    
        global output

        self.custom_cups_blue = SendVelVector()
        self.custom_cups_blue.finishX, self.custom_cups_blue.finishY, self.custom_cups_blue.vel, self.custom_cups_blue.move_angle = 0.1, 0.19, 0.15, 150.0 
        self.own_cups_blue = SendVelVector()
        self.own_cups_blue.finishX, self.own_cups_blue.finishY, self.own_cups_blue.vel, self.own_cups_blue.move_angle = 0.6, 1.9, 0.15, 150.0 
        self.flags_blue = SendVelVector()
        self.flags_blue.finishX, self.flags_blue.finishY, self.flags_blue.vel, self.flags_blue.move_angle = 0.8, 0.1, 0.15, 150.0 

        self.enemy.x, self.enemy.y = -1, -1

        odom_sub   = rospy.Subscriber("odom", Odometry,             self.odomCallback)
        enemy_sub  = rospy.Subscriber("aruco/robot4", PoseStamped,  self.enemyCallback)
        cups_sub   = rospy.Subscriber("mb/cups",      MaybeCupList, self.cupsCallback)
        while self.robot.x is None:
            rospy.sleep(0.1)

    def makePathPlan(self, startX, startY, goalX, goalY):
        plan = TPathPlan(startX, startY, goalX, goalY, len(self.pathPlan))
        self.pathPlan.append(plan)
        return plan

    """
    Gets list of cups from cups node
    """
    def cupsCallback(self, msg):
        self.rawCups = msg 
    """
    Get enemy`s robot position
    """
    def enemyCallback(self, msg):
        self.enemy.x = msg.pose.position.x
        self.enemy.y = msg.pose.position.y
    """
    Get odometry
    """
    def odomCallback(self, msg):
        self.robot.x = msg.pose.pose.position.x
        self.robot.y = msg.pose.pose.position.y
        self.robot.orient = msg.pose.pose.orientation
        (_, _, self.robot.theta) = quaternionToAngle([self.robot.orient.x, self.robot.orient.y, self.robot.orient.z, self.robot.orient.w])
        self.robot.theta = normalizeAngle(self.robot.theta)
        
  
    def vectorMove(self, point):
        vectorProxy = rospy.ServiceProxy("custom/send_vel_vector", SendVelVector)
        res = vectorProxy(point.finishX, point.finishY, point.vel, point.move_angle)
    
    def basicMove(self, plan, theta = None, cupId = None):
        tasks = []
        tasks.append(TVChange("initV", [], baseV = 0.4, minV = 0.15))
        tasks.append(TGlobalPlannerClient("getPath", [], plan))
        tasks.append(TGlobalPlannerUpdateCacheNext("cachedPath", []))
        if cupId is not None:
            tasks.append(TRemoveCup("removeCup", [], cupId))
        tasks.append(TPathToTrajectory("sendPath", ["getPath"]))
        tasks.append(TMoveToTrajectory("movePath", ["sendPath"]))
        if theta is not None:
            tasks.append(TThetaToTrajectory("sendTheta", ["sendPath"], degToRad(theta)))
        tasks.append(TLocalPlannerClient("local", ["sendPath"]))
        
        ctxData = dict()
        ctxData["scheduler"] = {"debug": False, "rootRate": 0.5, "rootControlEnabled": False}
        ctxData["reg"] = dict()
        ctxData["reg"]["moving"] = False
        ctxData["reg"]["self"] = self.robot
        ctxData["reg"]["enemy"] = self.enemy
        ctxData["reg"]["status"] = 0
        ctxData["reg"]["node"] = self
        ctxData["reg"]["actual_path"] = Path()
        ctxData["reg"]["global_path"] = Path()

        sched = TScheduler(TRuntimeContext(ctxData))

        for x in tasks:
            sched.addTask(x)
        sched.build()
        sched.start()
        return ctxData["reg"]["status"]
    
    def moveFromCur(self, ctx, plan, theta = None):
        ctx.data["reg"]["ready"] = False
        if ((plan.startX - self.robot.x) ** 2 + (plan.startY - self.robot.y) ** 2) ** 0.5 <= 0.25: 
            step = plan
        else:
            step = TPathPlan(self.robot.x, self.robot.y, plan.goalX, plan.goalY, -1)
        
        self.basicMove(step, theta)
        
        ctx.data["reg"]["ready"] = True

    """
    moveFromCurToCupByID - use moveFromCur, but path will finish near cup number [cupId]
    """
    def moveFromCurToCupById(self, plan, cupId):
        cupX, cupY = self.rawCups.cups[cupId - 1].x - 0.05, self.rawCups.cups[cupId - 1].y - 0.05
        self.addPointToSteps(cupX, cupY)
        if ((plan.startX - self.robot.x) ** 2 + (plan.startY - self.robot.y) ** 2) ** 0.5 <= 0.25: 
            step = TPathPlan(plan.startX, plan.startY, cupX, cupY, plan.planId)
        else:
            step = TPathPlan(self.robot.x, self.robot.y, cupX, cupY, plan.planId)
        return self.basicMove(step, None, cupId)
    
    def moveToCup(self, ctx, plan, cupId):
        self.moveFromCurToCupById(plan, cupId)
        ctx.data["reg"]["ready"] = True
    
    def moveToCupId(self, plan, cupId):
        x, y = self.rawCups.cups[cupId - 1].x, self.rawCups.cups[cupId - 1].y
        runWithController(self.moveToCup, [plan, cupId], {}, self.sideAimCtrl, [x, y, cupId], {})
  
    def moveToHomeBlue(self, ctx, plan):
        ctx.data["reg"]["ready"] = False
        if ((plan.startX - self.robot.x) ** 2 + (plan.startY - self.robot.y) ** 2) ** 0.5 <= 0.25: 
            step = TPathPlan(plan.startX, plan.startY, 0.28, 1.1, plan.planId)
        else:
            step = TPathPlan(self.robot.x, self.robot.y, 0.28, 1.1, plan.planId)
        self.basicMove(step, 90)
        ctx.data["reg"]["ready"] = True
    
    #TODO: add manipulator movement
    def raiseFlagsBlue(self, plan1, plan2):
        self.moveFromCur(plan1, 180.0)
        self.vectorMove(self.flags_blue)
        self.moveFromCur(plan2, 180.0)
    
    #TODO: add manipulator movement
    def takeOwnCupsBlue(self, plan1, plan2):
        self.moveFromCur(plan1, 0.0)
        self.vectorMove(self.own_cups_blue)
        self.moveFromCur(plan2, 0.0)
    
    #TODO: add manipulator movement
    def takeCustomCupsBlue(self, plan1, plan2):
        self.moveFromCur(plan1, 90.0)
        self.vectorMove(self.custom_cups_blue)
        self.moveFromCur(plan2, 90.0)
    
    """
    Maybe some test function.
    """
    # TODO: delete if unused
    def localPlannerTest(self, startX, startY, finishX, finishY):
        tasks = []

        tasks.append(TGlobalPlannerClient("getPath", [], (startX, startY), (finishX, finishY))) 
        tasks.append(TLocalPlannerClient("localPlanner", ["getPath"]))
    #   tasks.append(TMoveToTrajectory("movePath", ["sendPath"], degToRad(270.0))) 

        ctxData = dict()
        ctxData["scheduler"] = {"debug": False, "rootRate": 0.5, "rootControlEnabled": False}
        ctxData["reg"] = dict()

        sched = TScheduler(TRuntimeContext(ctxData))

        for x in tasks:
            sched.addTask(x)

        sched.build()
        sched.start()
   
    """
    Controller example: rotates robot while it is moving
    """
    def ctrlRotation(self, ctx): 
        theta = degToRad(270)
        while not ctx.data["reg"]["ready"]: 
            rospy.sleep(0.5)
            theta += degToRad(10)
            theta = normalizeAngle(theta)
            sendThetaToTrajectory(theta)

        print "I have killed Kenny"

    # TODO: delete if unused
    def calcAngle(self, vec):
        return normalizeAngle(degToRad(360.0) - atan2(vec.x, vec.y) - self.robot.theta)
    
    # TODO: remove bug 
    def sideAimCtrl(self, ctx, aimX, aimY, Id): 
        selfRobot  = self.robot 
        aimPoint   = TPoint2D(aimX, aimY)

        while not ctx.data["reg"]["ready"]:
            robotPoint = TPoint2D(selfRobot.x, selfRobot.y)
            toAim = vectorFromPoints(robotPoint, aimPoint)
            """
            if toAim.y >= 0:
                angle = normalizeAngle(degToRad(270) + atan2(toAim.x, toAim.y))
            else:
                angle = normalizeAngle(degToRad(270) + degToRad(180) + atan2(toAim.x, toAim.y))
            """
            ex = toAim.x / toAim.length()
            ey = toAim.y / toAim.length()
            #print toAim
            if ey > 0:
                angle = normalizeAngle(asin(-ex))
            else:
                angle = normalizeAngle(degToRad(180) - asin(-ex))
            #print radToDeg(angle)
            #print toAim
            #print radToDeg(atan2(toAim.x, toAim.y))

            x, y = self.rawCups.cups[Id].x, self.rawCups.cups[Id].y
            pub = rospy.Publisher('close_to_cup', Int16, queue_size=1)
            if ((x - robotPoint.x)**2 + (y - robotPoint.y)**2 >= infRad**2):
                pub.publish(0)
                sendThetaToTrajectory(angle)
            else:
                pub.publish(1)
            prevAngle = angle
            rospy.sleep(0.25)
   
    def addPointToSteps(self, pointX, pointY):
        point = TPoint2D(pointX, pointY)
        self.steps.append(point)
    
    def addCupToSteps(self, cupId):
        cupX, cupY = self.rawCups.cups[cupId - 1].x - 0.05, self.rawCups.cups[cupId - 1].y - 0.05
        self.addPointToSteps(cupX, cupY)

    def addFlagsToSteps(self):
        self.addPointToSteps(0.2, 0.1)
        self.addPointToSteps(0.8, 0.2)

    def addOwnCupsToSteps(self):
        self.addPointToSteps(1.2, 1.9)
        self.addPointToSteps(0.6, 1.8)

    def addCustomCupsToSteps(self):
        self.addPointToSteps(0.1, 0.64)
        self.addPointToSteps(0.3, 0.3)
    
    def addHomeBlueToSteps(self):
        self.addPointToSteps(0.24, 1.3)

    def printRobotState(self):
        output.write(str(self.robot.x) + '\t')
        output.write(str(self.robot.y) + '\t')
        output.write(str(radToDeg(self.robot.theta)))
        output.write("\n***\n")
        #print self.robot.x, self.robot.y, radToDeg(self.robot.theta)
        #print "*****"

    def checkDistanceToPoint(self, ctx, x, y):
        while not ctx.data["reg"]["ready"]:
            pub = rospy.Publisher('close_to_cup', Int16, queue_size=1)
            if ((x - self.robot.x)**2 + (y - self.robot.y)**2 >= infRad**2):
                pub.publish(0)
            else:
                pub.publish(1)
            #print ctx.data["reg"]["ready"]
        print "after stop", self.robot
        pub.publish(0)

    def start(self):
        self.addPointToSteps(1.5, 1.0)
        #self.addCupToSteps(14)
        #self.addCupToSteps(15)
        #self.addOwnCupsToSteps()
        self.addHomeBlueToSteps()
        
        for i in range (len(self.steps) - 1):
           self.root.append(self.makePathPlan(self.steps[i].x, self.steps[i].y, self.steps[i + 1].x, self.steps[i + 1].y))

        self.curPathPlan = 1
        
        for i in range (3):
            runWithController(self.moveFromCur, [self.root[0], 90], {}, self.checkDistanceToPoint, [self.root[0].goalX, self.root[0].goalY], {})
            #self.moveFromCur(self.root[0], 90)
        #self.moveToCupId(self.root[0], 14)
        #self.moveToCupId(self.root[1], 15)
            rospy.sleep(2.0)
            self.printRobotState()

            runWithController(self.moveToHomeBlue, [self.root[1]], {}, self.checkDistanceToPoint, [self.root[1].goalX, self.root[1].goalY], {})
            #self.moveToHomeBlue(self.root[1])
            #rospy.sleep(1.0)
        
        output.close()
root = TRootNode()
root.start()
