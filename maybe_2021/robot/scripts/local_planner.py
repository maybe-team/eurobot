#!/usr/bin/env python

import rospy

from math import cos

from maybe_lib.MaybeUtil import waitForPublisher
from maybe_lib.MaybeMath import TSegment, TPoint2D, TVector2D, vectorFromPoints, vectorPseudo, vectorProd, vectorCos, degToRad, radToDeg, rotateVector, distFromSegment

from maybe_lib.srv import CheckPath, LocalPath, LocalPathResponse
from maybe_lib.msg import MaybeCup, MaybeCupList

from cost_map_msgs.msg import CostMap
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path

rospy.init_node("local_planner")

class TLocalPlanner:
    def __init__(self):
        
        self.enemy = TPoint2D(0, 0)
        costmapSub   = rospy.Subscriber("cost_map_server/cost_map", CostMap, self.costmapCallback)
        enemySub     = rospy.Subscriber("aruco/robot4", PoseStamped, self.enemyCallback)
        cupsSub      = rospy.Subscriber("mb/cups", MaybeCupList, self.cupsCallback)
        
        self.rvzPub  = rospy.Publisher("planner/gp/path_full", Path, queue_size = 1)
        self.rvzPub2 = rospy.Publisher("/localpath", Path, queue_size = 1)
        
        #waitForPublisher(self.rvzPub)
        #waitForPublisher(self.rvzPub2)
        
        self.inflation = []
        """
        enemyRadius    - radius of enemy's robot
        enemyInflation - dangerous area around robot
        """
        self.enemyRadius = 0.4
        self.enemyInflation = 0.025

        self.rawCups = MaybeCupList()
    
    """
    detalizePoints - get 2 points and ADD MANY NEW POINTS between them
    """
    def detalizePoints(self, first, second, newPath, steps):
        if steps <= 0:
            return
        vec = vectorFromPoints(first, second)
        vec = vec * 0.5
        
        """
        middle is in the middle (wow) of [first, second] segment
        """
        middle = first + vec

        """
        simple recursion
        """
        self.detalizePoints(first, middle, newPath, steps - 1)
        self.detalizePoints(middle, second, newPath, steps - 1)

        newPath.append(second)
    
    """
    detalizePath - get path and applies detalizePoint to every consequent pair of points
    """
    def detalizePath(self, path, newPath, steps):
        for i in range(1, len(path)):
            self.detalizePoints(path[i - 1], path[i], newPath, steps)
    
    """
    simplifyPath - Ramer Douglas Peucker - gets path that consists of many points and simplifies it by deleting some points
    steps: int, depth of recursion. Less steps - faster and worse. More steps - slower and better
    Goooooooooooooooogle it!
    """
    def simplifyPath(self, path, newPath, eps, steps = 13):

        if steps <= 0 or len(path) <= 2:
            for x in path:
                newPath.append(x)
            return
        
        start = path[0]
        finish = path[-1]
        
        maxDst = -1
        index = -1

        for i in range(0, len(path)):
            x = path[i]
            startX = vectorFromPoints(start, x)
            startFinish = vectorFromPoints(start, finish)
            
            if startFinish.length() == 0.0:
                return

            dst = 0.5 * abs(vectorPseudo(startX, startFinish)) / startFinish.length() 
            if dst > maxDst:
                maxDst = dst
                index = i

        if maxDst >= eps: 
            part1 = path[0:index]
            part2 = path[index: -1]

            out1 = []
            out2 = []

            self.simplifyPath(part1, out1, eps, steps - 1)
            self.simplifyPath(part2, out2, eps, steps - 1)
            
            for x in out1:
                newPath.append(x)
            for x in out2:
                newPath.append(x) 
        else:
            newPath.append(start)
            newPath.append(finish)

    def enemyCallback(self, msg): 
        self.enemy.x = msg.pose.position.x
        self.enemy.y = msg.pose.position.y
    
    def cupsCallback(self, msg):
        self.rawCups = msg

    def costmapCallback(self, msg):
        inflationMsg = msg.data[1]
        inflationMsg.data = [ord(value) for value in inflationMsg.data]
        self.inflation = inflationMsg.data
        self.dimY = inflationMsg.layout.dim[0].size
        self.dimX = inflationMsg.layout.dim[1].size

    """
    pointToIndex - converts (x, y) coordinates to costmap index
    """
    def pointToIndex(self, x, y):
        x = int(x * 100)
        y = int(y * 100)
        return (self.dimX - x - 1) + (self.dimY - y - 1) * self.dimX

    """
    evadeRobot - gets detalizedPath, and evade enemy`s robot by geometry
    side: 1 or -1 (clockwise or counter-clockwise evade)
    """
    def evadeRobot(self, detalizedPath, correctedPath, side):
        insideRobot = False
        entryPoint = TPoint2D(0.0, 0.0)

        for i in range(0, len(detalizedPath)):
            p = detalizedPath[i]
            toEnemy = vectorFromPoints(p, self.enemy)
            """
            Check wether poing inside enemy robot inflation
            """
            if toEnemy.length() <= self.enemyRadius + self.enemyInflation:
                if not insideRobot:
                    entryPoint = p
                    
                insideRobot = True
            else:
                if insideRobot:
                    """
                    Here we construct evading path!
                    entryPoint - point on circle where we ENTERED enemy inflation
                    p          - point on circle where we EXITED  enemy inflation
                    """
                    vectorFromCenter = vectorFromPoints(self.enemy, entryPoint)  
                    
                    """
                    Draw a vector from (CENTER OF ENEMY) to (entryPoint)
                    Then rotate it by 1 degree till it hits (p) point (exit)
                    """
                    for i in range(1, 360):
                        rotatedVector = rotateVector(vectorFromCenter, side * degToRad(i))
                        pointOnCircle = self.enemy + rotatedVector
                        distToEnd = vectorFromPoints(pointOnCircle, p).length()
                        if distToEnd <= 0.1:
                            break
                        
                        correctedPath.append(pointOnCircle)
                        
                        
                insideRobot = False
                correctedPath.append(p)
        return
    """
    evadeCup - gets detalizedPath AFTER evading robots, evade one cup by geometry
    chooses shortest path
    """
    def evadeCup(self, detalizedPath, correctedPath, rawCup):
        entryPoint = TPoint2D(0.0, 0.0)
        insideCup = False
        

        """
        Works as evadeRobot but choses side in other way
        """
        for p in detalizedPath:
            cup = TPoint2D(rawCup.x, rawCup.y)
            toCup = vectorFromPoints(cup, p)
            toEnemy = vectorFromPoints(self.enemy, p)

            if toCup.length() <= rawCup.inflation and toEnemy.length() <= self.enemyRadius + self.enemyInflation + 0.05: 
                if not insideCup:
                    entryPoint = p
                insideCup = True
            else:
                if insideCup:
                    enemyToCup = vectorFromPoints(self.enemy, cup)
                    toEntryPoint = vectorFromPoints(cup, entryPoint)
                    if enemyToCup.length() <= rawCup.inflation + self.enemyRadius:
                        inflateCup = enemyToCup * (1.0 / enemyToCup.length()) * (rawCup.inflation)
                        projection = cup + inflateCup
                        """
                        Only difference
                        """
                        sign = cmp(vectorPseudo(toEntryPoint, inflateCup), 0)
                    
                    
                        for i in range(1, 360):
                            rotatedVector = rotateVector(toEntryPoint, sign * degToRad(i))
                            pointOnCircle = cup + rotatedVector
                            distToEnd = vectorFromPoints(pointOnCircle, p).length()
                            if distToEnd <= 0.1:
                                break
                        
                            correctedPath.append(pointOnCircle) 
                    
                    """
                    else:
                        circle1, circle2 = [], []
                        
                        for i in range(1, 360):
                            rotatedVector = rotateVector(toEntryPoint, degToRad(i))
                            pointOnCircle = cup + rotatedVector
                            distToEnd = vectorFromPoints(pointOnCircle, p).length()
                            if distToEnd <= 0.1:
                                break
                        
                            circle1.append(pointOnCircle) 
                        
                        for i in range(1, 360):
                            rotatedVector = rotateVector(toEntryPoint, -degToRad(i))
                            pointOnCircle = cup + rotatedVector
                            distToEnd = vectorFromPoints(pointOnCircle, p).length()
                            if distToEnd <= 0.1:
                                break
                        
                            circle2.append(pointOnCircle) 
                        
                        if len(circle1) > len(circle2):
                            circle1, circle2 = circle2, circle1

                        for x in circle1:
                            correctedPath.append(x)
                    """
                insideCup = False

                correctedPath.append(p)
        return

    """
    removeBadAngles - gets detalizedPath and remove all big angles
    """
    def removeBadAngles(self, detalizedPath, simplePath, minAngle):
        
        simplePath.append(detalizedPath[0])
        simplePath.append(detalizedPath[1])
        
        i = 2
        while i < len(detalizedPath):
            p = detalizedPath[i]
            frozenPoint = simplePath[-1]
            frozenPoint2 = simplePath[-2]

            fromFrozen = vectorFromPoints(frozenPoint, p)
            prev = vectorFromPoints(frozenPoint2, frozenPoint)
            
            cosDiff = vectorCos(prev, fromFrozen)

            while cosDiff <= cos(minAngle) and len(simplePath) > 2:

                simplePath.pop()
                
                frozenPoint = simplePath[-1]
                frozenPoint2 = simplePath[-2]
                
                fromFrozen = vectorFromPoints(frozenPoint, p)
                prev = vectorFromPoints(frozenPoint2, frozenPoint)

                cosDiff = vectorCos(prev, fromFrozen)
            
            simplePath.append(p)
            i += 1
    
    def pathLen(self, path):
        length = 0
        
        for i in range(1, len(path.poses)):
            p1 = path.poses[i - 1].pose.position
            p2 = path.poses[i].pose.position

            p1 = TPoint2D(p1.x, p1.y)
            p2 = TPoint2D(p2.x, p2.y)

            part = vectorFromPoints(p1, p2).length()

            length += part

        return length
    
    def pathOk(self, path):
        cnt = 0
        for x in path.poses: 
            p = x.pose.position
            index = self.pointToIndex(p.x, p.y)
            if index < len(self.inflation) and self.inflation[index] >= 6:
                cnt += 1
        print cnt
        return cnt < 5

    def handleCheckPath(self, req):
        ok = self.pathOk(req.plan)
        return ok
    
    """
    makes all possible paths and selects the best
    """
    def improvePath(self, req):
        result1 = self.improvePathVariant(req, 1)
        result2 = self.improvePathVariant(req, -1)
        
        ok1  = self.pathOk(result1.result)
        ok2  = self.pathOk(result2.result)

        if not ok1 and not ok2:
            print "fuck local planner"
            # self.rvzPub.publish(result2.result)
            # self.rvzPub2.publish(result1.result)
            result1.status = 1
            result1.length = -1
            return result1
        
        if not ok1 and ok2:
            print "1 is govno, chose 2"
            self.rvzPub.publish(result2.result)
            # self.rvzPub2.publish(result1.result)
            result2.length = self.pathLen(result2.result)
            return result2

        if not ok2 and ok1:
            print "2 is govno, chose 1"
            self.rvzPub.publish(result1.result)
            # self.rvzPub2.publish(result2.result)
            result1.length = self.pathLen(result1.result)
            return result1
        
        len1 = self.pathLen(result1.result)
        len2 = self.pathLen(result2.result)
        
        print len1, "?", len2

        if len1 < len2:
            print "1"
            self.rvzPub.publish(result1.result)
            # self.rvzPub2.publish(result2.result)
            result1.length = len1
            return result1
        else:
            print "2"
            self.rvzPub.publish(result2.result)
            # self.rvzPub2.publish(result1.result)
            result2.length = len2
            return result2
    
    """
    makes path with fixed parameters
    side: -1 or 1 (clockwise or counter-clockwise evading robot
    """
    # TODO: add new param enemy_robot (to evade both robots)
    def improvePathVariant(self, req, side):
        purePath = []
    
        for p in req.plan.poses:
            x, y = p.pose.position.x, p.pose.position.y
            purePath.append(TPoint2D(x, y))
        

        """
        Select cups that are near enemy robot (because they are most dangerous)
        """
        unorderedCups = []
        nearCups = []
        for rawCup in self.rawCups.cups:
            cup = TPoint2D(rawCup.x, rawCup.y)
            for i  in range(1, len(purePath)):
                fp = purePath[i - 1]
                sp = purePath[i]
                segment = TSegment(fp, sp)

                toCupDist = distFromSegment(segment, cup)
                toEnemyDist = distFromSegment(segment, self.enemy)
                
                if toCupDist <= 2 * rawCup.inflation and toEnemyDist <= self.enemyRadius:
                    unorderedCups.append((toCupDist, rawCup))
                    break
        
        """
        Sort cups by dist to enemy robot
        We need this to correctly evade cups
        """
        unorderedCups.sort()
        for x in unorderedCups:  
            nearCups.append(x[1])

        detalizedPath = []
        
        self.detalizePath(purePath, detalizedPath, 6)
        """
        Evade robot && store new path in [correctedPath]
        """
        correctedPath = []
        self.evadeRobot(detalizedPath, correctedPath, side)
        
        """
        Evade every near cup && store new path in [correctedPath]
        """
        for rawCup in nearCups: 
            toFirst = vectorFromPoints(TPoint2D(rawCup.x, rawCup.y), purePath[0])
            toLast  = vectorFromPoints(TPoint2D(rawCup.x, rawCup.y), purePath[-1])
            minDist = min(toLast.length(), toFirst.length())  
            if minDist >= rawCup.inflation:
                detalizedPath = []
                detalizedPath, correctedPath = correctedPath, detalizedPath  
                self.evadeCup(detalizedPath, correctedPath, rawCup)
            
        """
        detalizedPath = []
        detalizedPath, correctedPath = correctedPath, detalizedPath
 
        self.removeBadAngles(detalizedPath, correctedPath, degToRad(90))
        """
        
        """
        Make sure that finish point is in new path
        """
        correctedPath.append(purePath[-1])
        
        """
        Delete useless points
        """
        simplePath = []
        self.simplifyPath(correctedPath, simplePath, 0.0025) 
        simplePath.append(purePath[-1])

        detalizedPath = []
        detalizedPath, simplePath = simplePath, detalizedPath 
        
        """
        Remove bad angles
        minAngle - mininal angle that will be in new path (others will be deleted)
        """
        minAngle = degToRad(60)
        self.removeBadAngles(detalizedPath, simplePath, minAngle) 
        simplePath.append(purePath[-1])

        detalizedPath = []
        detalizedPath, simplePath = simplePath, detalizedPath 
        
        """
        Do it again, but from the other side (it makes difference!)
        """
        detalizedPath.reverse()

        self.removeBadAngles(detalizedPath, simplePath, minAngle) 
        simplePath.reverse()
        simplePath.append(purePath[-1])

        complexPath = Path()
        complexPath.header = req.plan.header
        
        complexPath2 = Path()
        complexPath2.header = req.plan.header

        """
        Transforming path to nav_msgs/Path format
        """
        for p in correctedPath:
            point = PoseStamped()
            point.pose.position.x = p.x
            point.pose.position.y = p.y
            complexPath2.poses.append(point)
        
        for p in simplePath:
            point = PoseStamped()
            point.pose.position.x = p.x
            point.pose.position.y = p.y
            complexPath.poses.append(point)
        
        res = LocalPathResponse()
        res.result = complexPath
        res.status = 0
 
        return res

localPlanner = TLocalPlanner()
improveService = rospy.Service("/mb/planner/local", LocalPath, localPlanner.improvePath)
checkPathServivce = rospy.Service("mb/planner/local/check", CheckPath, localPlanner.handleCheckPath)

print "local planner started"

rospy.spin()
