#!/usr/bin/env python
import rospy
import roslib
import tf
from std_msgs.msg import String
from std_msgs.msg import Float64
from std_msgs.msg import Bool
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from geometry_msgs.msg import Transform, Quaternion

#mag_msg = MagneticField()
imu_msg = Imu()
imu_broadcaster = tf.TransformBroadcaster()

node = rospy.init_node('mpu9250',anonymous=True)

pub = rospy.Publisher('/imu_data',Imu, queue_size=1)
#pub1 = rospy.Publisher('/imu/mag',MagneticField, queue_size=1)

def serial_callback(data):
    global imu_msg#, mag_msg
    #acc_mag_gyr = [float(i) for i in data.data.split(',')]

    imu_msg.header.stamp = rospy.Time.now()
    imu_msg.header.frame_id = "base_link"
    imu_msg.orientation.x = -data.x
    imu_msg.orientation.y = -data.y
    imu_msg.orientation.z = -data.z
    imu_msg.orientation.w = data.w
    '''imu_msg.orientation.x = data.rotation.x
    imu_msg.orientation.y = data.rotation.y
    imu_msg.orientation.z = data.rotation.z
    imu_msg.orientation.w = data.rotation.w

    mag_msg.header.stamp = rospy.Time.now()
    mag_msg.magnetic_field.x = data.translation.x
    mag_msg.magnetic_field.y = data.translation.y
    mag_msg.magnetic_field.z = data.translation.z'''


    pub.publish(imu_msg)
    #pub1.publish(mag_msg)


    '''imu_msg.angular_velocity.x = acc_mag_gyr[0]
    imu_msg.angular_velocity.y = acc_mag_gyr[1]
    imu_msg.angular_velocity.z = acc_mag_gyr[2]
    imu_msg.angular_velocity_covariance[0] = -1

    mag_msg = MagneticField()
    mag_msg.header.stamp = rospy.Time.now()
    mag_msg.magnetic_field.x = acc_mag_gyr[3]
    mag_msg.magnetic_field.y = acc_mag_gyr[4]
    mag_msg.magnetic_field.z = acc_mag_gyr[5]

    imu_msg.linear_acceleration.x = acc_mag_gyr[6]
    imu_msg.linear_acceleration.y = acc_mag_gyr[7]
    imu_msg.linear_acceleration.z = acc_mag_gyr[8]
    imu_msg.linear_acceleration_covariance[0] = -1'''

def listener():

    sub = rospy.Subscriber('quat', Quaternion ,serial_callback,queue_size=1)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        rate.sleep()


if __name__=='__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        print('Something Went wrong')
    pass
