#!/usr/bin/env python
import rospy
from std_msgs.msg import Int8
from main_robot.msg import servo_msg
from serial_overlay.srv import SetServo
from maybe_lib.msg import ServoPoses
import sys, select, os
if os.name == 'nt':
  import msvcrt
else:
  import tty, termios

idd = 0
speed = 5
poses = ServoPoses()

rospy.init_node('main_robot_teleop_manipulator')

servo_poses_pub = rospy.Publisher("/servo/poses", ServoPoses, queue_size = 1)

servo_send_proxy = rospy.ServiceProxy("serial_overlay/set_servo", SetServo)
control_servo = SetServo()
control_servo.vel = 300

def getKey():
    if os.name == 'nt':
      return msvcrt.getch()
    else:
        settings = termios.tcgetattr(sys.stdin)

    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 20)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''
        
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)

    return key

def servo_poses_sub_callback(msg):
    global poses
    for i in range (10):
        poses.poses[i] = msg.poses[i]


servo_poses_sub = rospy.Subscriber("/servo/poses", ServoPoses, servo_poses_sub_callback)

while not rospy.is_shutdown():
    key = getKey()
    if key == '' :
        pass

    elif key == '1' :
        idd = 1

    elif key == '3' :
        idd = 3

    elif key == '4' :
        idd = 4

    elif key == '5' :
        idd = 5

    elif key == '6' :
        idd = 6

    elif key == 'z' :
        speed += 1
        print "speed = ", speed

    elif key == 'x' :
        speed -= 1
        print "speed = ", speed

    elif key == 'w' :
        control_servo.id = idd
        if poses.poses[idd] < 1023:
            poses.poses[idd] += speed
        else: 
            poses.poses[idd] = 1023
        control_servo.pos = poses.poses[idd]
        servo_poses_pub.publish(poses)
        servo_send_proxy(control_servo.id, control_servo.pos, control_servo.vel)

    elif key == 's' :
        control_servo.id = idd
        if poses.poses[idd] > 0:
            poses.poses[idd] -= speed
        else: 
            poses.poses[idd] = 0
        control_servo.pos = poses.poses[idd]
        servo_poses_pub.publish(poses)
        servo_send_proxy(control_servo.id, control_servo.pos, control_servo.vel)


    else:
            if (key == '\x03'):
                break

rospy.spin()
