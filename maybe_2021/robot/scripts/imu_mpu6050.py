#!/usr/bin/env python
import rospy
import roslib
import tf
from std_msgs.msg import Float64, Bool
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from geometry_msgs.msg import Quaternion, Accel

imu_msg = Imu()
imu_broadcaster = tf.TransformBroadcaster()
acc_mag_gyr = [0] * 10

node = rospy.init_node('mpu9250',anonymous=True)

pub = rospy.Publisher('/imu', Imu, queue_size=1)

def accelgyro_callback(data):
    global acc_mag_gyr
    acc_mag_gyr[0] = data.linear.x
    acc_mag_gyr[1] = data.linear.y
    acc_mag_gyr[2] = data.linear.z
    acc_mag_gyr[3] = data.angular.x
    acc_mag_gyr[4] = data.angular.y
    acc_mag_gyr[5] = data.angular.z


sub = rospy.Subscriber('imu_arduino', Accel, accelgyro_callback)
rate = rospy.Rate(200)

def listener():
    global imu_msg, acc_mag_gyr

    imu_msg.header.stamp = rospy.Time.now()
    imu_msg.header.frame_id = "base_link"
    imu_msg.linear_acceleration.x = acc_mag_gyr[0]
    imu_msg.linear_acceleration.y = acc_mag_gyr[1]
    imu_msg.linear_acceleration.z = acc_mag_gyr[2]
    imu_msg.angular_velocity.x = acc_mag_gyr[3]
    imu_msg.angular_velocity.y = acc_mag_gyr[4]
    imu_msg.angular_velocity.z = acc_mag_gyr[5]
    imu_msg.orientation_covariance[0] = imu_msg.orientation_covariance[4] = 2.6030820491461885e-07
    imu_msg.angular_velocity_covariance[0] = imu_msg.angular_velocity_covariance[4] = imu_msg.angular_velocity_covariance[8] = 2.5e-05
    imu_msg.linear_acceleration_covariance[0] = imu_msg.linear_acceleration_covariance[4] = imu_msg.linear_acceleration_covariance[8] = 2.5e-05

    pub.publish(imu_msg)
    rate.sleep()


while not rospy.is_shutdown():
    listener()

