#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64, String
from sensor_msgs.msg import Imu

rospy.init_node('imu')

class TImu:
    def __init__(self):
        self.imu_msg = Imu()
        imu_sub = rospy.Subscriber('/imu/data', Imu, imu_callback)
        while not rospy.is_shutdown():
            rospy.sleep(0.1)
        
    def imu_callback(self, msg):
        self.imu_msg = msg
        """
        self.imu_msg.orientation.x = msg.orientation.x
        self.imu_msg.orientation.y = msg.orientation.y
        self.imu_msg.orientation.z = msg.orientation.z
        self.imu_msg.orientation.w = msg.orientation.w
        self.imu_msg.angular_velocity.x = msg.angular_velocity.x
        self.imu_msg.angular_velocity.y = msg.angular_velocity.y
        self.imu_msg.angular_velocity.z = msg.angular_velocity.z
        self.imu_msg.linear_acceleration.x = msg.linear_acceleration.x
        self.imu_msg.linear_acceleration.y = msg.linear_acceleration.y
        self.imu_msg.linear_acceleration.z = msg.linear_acceleration.z
        """

rospy.spin()
