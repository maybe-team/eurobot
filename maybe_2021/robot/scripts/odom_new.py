#! /usr/bin/env python
import rospy
import tf
import time
from serial_overlay.srv import SetCoord
from geometry_msgs.msg import Quaternion
from nav_msgs.msg import Odometry

deg_to_rad = 0.0174532

rospy.init_node('tf_odom')

setCoord = rospy.ServiceProxy("serial_overlay/set_coord", SetCoord)
x = rospy.get_param('global_x')
y = rospy.get_param('global_y')
theta = rospy.get_param('theta') * deg_to_rad
setCoord(x, y, theta)

odom_broadcaster = tf.TransformBroadcaster()

def odometry_callback(msg):
    current_time = rospy.Time.now()
    odom_quat = [0] * 4
    odom_quat[0] = msg.pose.pose.orientation.x 
    odom_quat[1] = msg.pose.pose.orientation.y 
    odom_quat[2] = msg.pose.pose.orientation.z 
    odom_quat[3] = msg.pose.pose.orientation.w 
    odom_broadcaster.sendTransform(
        (msg.pose.pose.position.x, msg.pose.pose.position.y, 0.0),
        odom_quat,
        current_time,
        "base_link",
        "odom"
    )

odometry = rospy.Subscriber('odom', Odometry, odometry_callback)
rospy.spin()
