#!/usr/bin/env python

import rospy
import time
from nav_msgs.msg import Odometry, Path
from geometry_msgs.msg import Twist, Pose2D, PoseWithCovarianceStamped, PoseStamped
from main_robot.msg import  motors_speeds
from tf.transformations import euler_from_quaternion
from std_msgs.msg import Float64, Int8
from math import atan2, hypot, pi, cos
rospy.init_node('trajectory_regulator')
global_coordinates = Pose2D()
aim_coordinates = Pose2D()

speed_motor = motors_speeds()

minimal_speed = 0.13 #rospy.get_param('minimal_speed')
slow_down_speed = 0.4
max_angle_speed = 0.68
maximum_speed = 0.7  #rospy.get_param('maximum_speed')

trajectory_accel = 0.0033
trajectory_breaking = -0.003

trajectory_breaking_dynamic = -0.00045
trajectory_accel_dynamic = 0.0006

p2p_breaking = -0.004

deg_to_rad = 0.0174532
rad_to_deg = 57.29577

dynamic_coord_eps = 0.07
static_coord_eps = 0.02
dstop_time = 0.2

error_eps = 0.0
angle_eps = 5

side_speed_value = 1.0
error_value = 0.75
odom_value = 0.0

slow_down_time = 0.55 / maximum_speed
speed_up_time = 0.05 / maximum_speed
finish_time = 0.65 / maximum_speed

slight_turn_dist = 0.7 

slight_turn_part = 0.5

need_to_rotate = False
ban_rotation = False
slight_turn = True

static_rotation_speed = 0.05

possibility_to_go = 1
side_speed = 0
side_speed_sign = 1

error_len = 0
path = []
current_index = 0


velocity = minimal_speed
accel = 0.0
breaking = 0.0

counter = 0
limit = 500
can_move = False

approx_dist = 0.0
odom_v = 0.0

draw_counter = 0
draw_rate = 17

real_path = Path()

motors = rospy.Publisher('motor_power', motors_speeds, queue_size=1)
cmd_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
side_setpoint = rospy.Publisher('side/setpoint' , Float64, queue_size=1)
side_input = rospy.Publisher('side/state' , Float64, queue_size=1)

real_path_pub = rospy.Publisher('/realpath', Path, queue_size = 1)
test_est_pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size = 1)

error_setpoint = rospy.Publisher('/error/setpoint', Float64, queue_size=1)
error_state    = rospy.Publisher('/error/state', Float64, queue_size=1)

def callback_odom (msg):
    global velocity, odom_v, global_coordinates, real_path, real_path_pub, draw_counter
    global_coordinates.x = msg.pose.pose.position.x
    global_coordinates.y = msg.pose.pose.position.y
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, global_coordinates.theta) = euler_from_quaternion (orientation_list)
    global_coordinates.theta = normalize_angle(global_coordinates.theta * rad_to_deg)

    draw_counter += 1
    v_x = msg.twist.twist.linear.x
    v_y = msg.twist.twist.linear.y
    
    odom_v = hypot(v_x, v_y)
    
    # velocity = odom_value * odom_v + (1 - odom_value) * velocity
    """
    if odom_v != 0.0 and velocity != 0.0:
        print "odom_v", odom_v
        print "aim v", velocity
    """
    if draw_counter > draw_rate:
        new_coord = PoseStamped()
        new_coord.pose.position.x = global_coordinates.x
        new_coord.pose.position.y = global_coordinates.y
    
        real_path.poses.append(new_coord)
        real_path.header.frame_id = "map"    
        real_path_pub.publish(real_path)
        draw_counter = 0

    if len(path) > 1:
        # print "LONG PATH", path
        trajectory_move(path[-1][0], path[-1][1], global_coordinates.x, global_coordinates.y, path[-2][0], path[-2][1], global_coordinates.theta, len(path) <= 2)
    elif len(path) == 1:
        # print "SHORT PATH", path
        p2p_move(global_coordinates.x, global_coordinates.y, aim_coordinates.x, aim_coordinates.y, global_coordinates.theta)
    else:
        # print "NO PATH"
        can_move = False
        motors_stop()

def callback_trajectory_regulator(msg):
    global aim_coordinates
    aim_coordinates.x = msg.x
    aim_coordinates.y = msg.y
    aim_coordinates.theta = msg.theta
    go_line(aim_coordinates.x, aim_coordinates.y, aim_coordinates.theta)

def callback_output(msg):
    global side_speed, side_speed_sign
    raw_speed = abs(msg.data)
    if slight_turn: 
        raw_speed = slight_turn_part * raw_speed
    side_speed = side_speed_sign * raw_speed

    if need_to_rotate:
        side_speed = static_rotation_speed * side_speed_sign
    if False and ban_rotation:
        side_speed = 0.0

def normalize_angle(angle):
    while angle >= 360:
        angle -= 360
    while angle < 0:
        angle += 360
    return angle

def diff_angle(cur, now):
    diff1 = normalize_angle(cur - now)
    diff2 = normalize_angle(now - cur)
    return min(diff1, diff2)

def motors_pub(speed, global_angle):
    global side_speed, speed_motor
    speed_motor.motor1 = speed * (cos((90 - global_angle) * deg_to_rad)) + side_speed
    speed_motor.motor2 = speed * (cos((210 - global_angle) * deg_to_rad)) + side_speed
    speed_motor.motor3 = speed * (cos((330 - global_angle) * deg_to_rad)) + side_speed
    motors.publish(speed_motor)

def motors_stop():
    global speed_motor, velocity
    velocity = 0.0
    speed_motor.motor1 = 0
    speed_motor.motor2 = 0
    speed_motor.motor3 = 0
    motors.publish(speed_motor)

def callback_possibility(msg):
    possibility_to_go = msg.data

def callback_error(msg):
    global error_len
    error_len = abs(msg.data)

def callback_get_path(msg):
    global path, side, can_move, counter, aim_coordinates, slight_turn, approx_dist
    error_setpoint.publish(0.0)
    new_path = [(x.pose.position.x, x.pose.position.y) for x in msg.poses] 
    # print "new path"
    to_begin = (global_coordinates.x - new_path[-1][0], global_coordinates.y - new_path[-1][1])
    approx_dist = (new_path[0][0] - new_path[-1][0], new_path[0][1] - new_path[-1][1])
    if hypot(to_begin[0], to_begin[1]) < 0.2:
        # print "approx dist", hypot(approx_dist[0], approx_dist[1])
        if approx_dist < slight_turn_dist:
            # print "Fast turn"
            slight_turn = False
        else:
            # print "Slow turn"
            slight_turn = True
        # print "we are near start, can move"
        # print new_path 
        can_move = True
        need_to_rotate = False
        ban_rotation = False
        counter = 0
        velocity = minimal_speed
        path = new_path
        orientation = msg.poses[0].pose.orientation
        aim_theta = euler_from_quaternion([orientation.x, orientation.y, orientation.z, orientation.w])[2]
        aim_coordinates.theta = normalize_angle(aim_theta * rad_to_deg - 90)
        aim_coordinates.x = path[0][0]
        aim_coordinates.y = path[0][1]
        # print "aim", aim_coordinates.x, aim_coordinates.y
        # print "aim_theta", aim_coordinates.theta
        side_setpoint.publish(aim_coordinates.theta) 
    else:
        pass
        # print "me: ", global_coordinates.x, global_coordinates.y
        # print "aim:", new_path[-1][0], new_path[-1][1]
        # print "start is far away"

def calc_velocity(accel, min_v, max_v):
    global velocity
    velocity += accel
    side_correct = (abs(side_speed) ** 1.5) * side_speed_value
    min_v = max(0.0, min_v - side_correct)
    max_v = max(0.0, max_v - side_correct)
    velocity = max(min_v, velocity)
    velocity = min(max_v, velocity)

def calc_accel(daccel, min_a, max_a):
    global accel
    accel += daccel 
    accel = max(min_a, accel)
    accel = min(max_a, accel)

def calc_breaking(dbreaking, min_b, max_b):
    global breaking
    breaking += dbreaking 
    breaking = max(min_b, breaking)
    breaking = min(max_b, breaking)


def avg_vec(vec_1, vec_2):
    return ((vec_1[0] + vec_2[0]) / 2, (vec_1[1] + vec_2[1]) / 2)

def ort_vec(vec):
    if vec[0] == 0.0:
        return (cmp(vec[1], 0), 0.0)
    if vec[1] == 0.0:
        return (0.0, cmp(0, vec[0]))

    ort = (-(vec[1] / vec[0]), 1)
    ort = (ort[0] * cmp(0, vec[0]), ort[1] * cmp(0, vec[0]))

    return ort
    


def calc_angle(vec, theta):
    return normalize_angle(360 - atan2(vec[0], vec[1]) * rad_to_deg - theta)

def p2p_move(cur_x, cur_y, finish_x, finish_y, theta):
    global counter, path, can_move, current_index, side_speed_sign
    side_setpoint.publish(aim_coordinates.theta)

    rest_vector = (finish_x - cur_x, finish_y - cur_y)
    
    rest_angle = calc_angle(rest_vector, theta)
    rest_dist = hypot(rest_vector[0], rest_vector[1])
    theta_diff = diff_angle(theta, aim_coordinates.theta)
    
    rest_coef = (odom_v * dstop_time) / rest_dist
    rest_norm = (rest_vector[0] * rest_coef, rest_vector[1] * rest_coef)
    
    check_vector = (finish_x - cur_x -  rest_norm[0], finish_y - cur_y - rest_norm[1])
    check_dist = hypot(check_vector[0], check_vector[1])

    counter += 1
    
    # print "P2P"
    
    if not can_move:
        # print "I need to stop!"
        # print "Total angle eps", theta_diff
        # print "Total dist eps", rest_dist
        motors_stop()
        return  
    
    if counter > limit:
        print "P2P LIMIT"
        can_move = False
        motors_stop()
        return

    finish_dist = finish_time * odom_v

    if rest_dist >= finish_dist and False:
        print "WE ARE FAR AWAY"
        motors_stop()
        can_move = False
        return

    if check_dist < static_coord_eps and theta_diff < angle_eps:
        print "GET!"
        print "I need to stop!"
        print "Total angle eps", theta_diff
        print "Total dist eps", rest_dist
        can_move = False
        path = []
        current_index += 1
        motors_stop()
        return
    
    if normalize_angle(aim_coordinates.theta - theta) > normalize_angle(theta - aim_coordinates.theta):
        side_speed_sign = 1
    else: 
        side_speed_sign = -1
    
    aim_velocity = 0.0
    accel = 0.0
    
    if theta_diff < angle_eps:
        # print "good angle", theta_diff
        side_input.publish(aim_coordinates.theta)
    else:
        # print "bad angle", theta_diff
        side_input.publish(theta)
    
    if check_dist < static_coord_eps:
        # print "good dist", rest_dist
        aim_velocity = 0.0
        need_to_rotate = True
    else:
        aim_velocity = minimal_speed
        # print "bad dist", rest_dist
        need_to_rotate = False
    
    calc_breaking(trajectory_breaking_dynamic, p2p_breaking, 0.0)
    calc_accel(-trajectory_accel_dynamic, 0.0, trajectory_accel)
    calc_velocity(breaking, aim_velocity, max(velocity, aim_velocity))
    # print "breaking", breaking
    # print "velocity", odom_v
    motors_pub(velocity, rest_angle)
        

def trajectory_move(start_x, start_y, cur_x, cur_y, finish_x, finish_y, theta, last_point = False):
    global minimal_speed, maximum_speed, can_move, velocity, accel, breaking, counter, path, current_index, aim_coordinates, need_to_rotate, ban_rotation, error_len, side_speed_sign 
    counter += 1 
    if counter > limit:
        # print "TRAJ LIMIT"
        can_move = False
        motors_stop()
        return

    vector_to = (finish_x - cur_x, finish_y - cur_y)
    vector_from = (cur_x - start_x, cur_y - start_y)
    vector_to_aim = (cur_x - aim_coordinates.x, cur_y - aim_coordinates.y)

    total_vector = (finish_x - start_x, finish_y - start_y)
    ort_total = ort_vec(total_vector)
     

    error = 0.5 * (total_vector[0] * vector_to[1] - total_vector[1] * vector_to[0]) / hypot(total_vector[0], total_vector[1])
     
    ort_coef = (cmp(0, error) * error_len * error_value) / hypot(ort_total[0], ort_total[1]) 
    total_coef = 1.0 / hypot(total_vector[0], total_vector[1])
    
    ort_norm = (ort_total[0] * ort_coef, ort_total[1] * ort_coef)
    total_norm = (total_vector[0] * total_coef, total_vector[1] * total_coef)

    real_vector = avg_vec(total_norm, ort_norm) 
     

    local_angle = calc_angle(vector_to, theta)
    perfect_angle = calc_angle(total_vector, theta)
    ort_angle = calc_angle(ort_total, theta)
    real_angle = calc_angle(real_vector, theta)

    angle = real_angle 
 
    rest_dist = hypot(vector_to[0], vector_to[1])     
    done_dist = hypot(vector_from[0], vector_from[1])
    total_dist = hypot(total_vector[0], total_vector[1])
    aim_dist = hypot(vector_to_aim[0], vector_to_aim[1])
    
    if not can_move:
        motors_stop()
        return

    """ 
    print "aim theta", aim_coordinates.theta
    print "my  theta", theta
    print "v        ", 
    print "diff1", normalize_angle(aim_coordinates.theta - theta)
    print "diff2", normalize_angle(theta - aim_coordinates.theta)
    """
    side_setpoint.publish(aim_coordinates.theta)
    if normalize_angle(aim_coordinates.theta - theta) > normalize_angle(theta - aim_coordinates.theta):
        # print "AAAAAAAAAAAAA"
        side_speed_sign = 1
    else:
        # print "BBBBBBBBBBBB"
        side_speed_sign = -1
    
    side_input.publish(theta)
 
    aim_velocity = 0.0
    
    slow_down_dist = slow_down_time * odom_v
    finish_dist = finish_time * odom_v

    # print slow_down_dist
    print finish_dist
    print aim_dist

    if total_dist < slow_down_dist or rest_dist < slow_down_dist or aim_dist < finish_dist:
        print "slow"
        if aim_dist < finish_dist:
            print "aim finish"
            if rest_dist >= slow_down_dist:
                print "its ok!!!"
        if (last_point and rest_dist < slow_down_dist) or aim_dist < finish_dist:
            print "minimal_speed", velocity
            aim_velocity = minimal_speed
            
        else:
            #print "slow_down_speed"
            aim_velocity = slow_down_speed

    else:
        print "fast"
        if True or abs(local_angle - 90.0) < 45.0 or abs(local_angle - 270.0) < 45.0:
            #print "max_angle_speed"
            aim_velocity = max_angle_speed

        else:
            #print "maximum_speed"
            aim_velocity = maximum_speed


    # print odom_v
    # print accel, breaking

    error_state.publish(abs(error) if abs(error) >= error_eps else 0.0)

    if rest_dist < dynamic_coord_eps and not last_point:
        # print "DEL POINT"
        path = path[0:-1]
        current_index += 1 

    if rest_dist < finish_dist and last_point:
        # print "FINISH"
        need_to_rotate = False
        ban_rotation = False
        error_state.publish(0.0)
        path = path[0:-1]
        current_index += 1 
        p2p_move(cur_x, cur_x, finish_x, finish_y, theta)
    elif not rest_dist < dynamic_coord_eps:
        if velocity > aim_velocity:
            calc_breaking(trajectory_breaking_dynamic, trajectory_breaking, 0.0)
            calc_accel(-trajectory_accel_dynamic, 0.0, trajectory_accel)
            calc_velocity(breaking, aim_velocity, max(velocity, aim_velocity))
            if last_point and velocity > 0:
                ban_rotation = True
        elif velocity < aim_velocity:
            calc_breaking(-trajectory_breaking_dynamic, trajectory_breaking, 0.0)
            calc_accel(trajectory_accel_dynamic, 0.0, trajectory_accel)
            
            calc_velocity(accel, min(aim_velocity, velocity), aim_velocity)
        
        motors_pub(velocity, angle)
            
        

odom_sub = rospy.Subscriber("odom", Odometry, callback_odom)
trajectory_regulator_sub = rospy.Subscriber("trajectory_regulator", Pose2D, callback_trajectory_regulator)

side_output = rospy.Subscriber('side/control_effort', Float64, callback_output)

simple_path_sub = rospy.Subscriber('/planner/gp/path_simple', Path, callback_get_path)
error_effort   = rospy.Subscriber('/error/control_effort', Float64, callback_error)

#possibility_to_go = rospy.Service('possibility_to_go', Int8, callback_possibility)
r = rospy.Rate(30)



rospy.spin()
