# Структура


- global_planner, cost_map: roscpp (код CберТеха)

- maybe_lib: rospy 
    - MaybeMath.py: математика
    - MaybeScheduler.py: планировщик
    - MaybeEntity.py: Python классы (пока там только класс TRobot)

- robot2: робот с манипулятором-присоской (rospy)
    - adv_traj.py - траекторный регулятор
    - local_planner.py - локальный планировщик
    - cups.py - менеджер стаканчиков
    - smart_position.py - маяковая система (нужно переписать)
    - teleop_key.py - ручное управление
    - root_node.py - головная нода

- serial_overlay: коммуникация с STM (rospp)
    - serial_overlay_node.cpp - сервисы общения с STM

Перед запуском нужно (запустить алиасы 1 и 2 на роботе), запустить rviz на ПК

Для того, чтобы вручную запустить верхний уровень нужно (одновременно)

```console
foo@bar:~$ rosrun robot2 adv_traj.py
```

```console
foo@bar:~$ rosrun robot2 local_planner.py
```

```console
foo@bar:~$ rosrun robot2 cups.py
```

По желанию (лучше сделать)
```console
foo@bar:~$ rosrun robot2 smart_position.py
```

ПОСЛЕ ВСЕГО ЭТОГО (убедившись, что все остальное корректно запустилось)
```console
foo@bar:~$ rosrun robot2 root_node.py
```
